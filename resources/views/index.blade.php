<!doctype html>
<html lang="{{ app()->getLocale() }}}">
<head>
    @include('layouts.head')
</head>
<body>
@if(isset($_SESSION['username']))
        <div class="pages">
            @include('components.header')
            @include('pages.index')

            @include('components.delete-ad')
            @include('components.popup')
        </div>
        @include('components.footer')
@else
    <div class="pages">
        @include('components.headerLogin')
        @include('pages.indexLogin')

        @include('components.login')
        @include('components.registration')
        @include('components.recovery')
        @include('components.popup')
    </div>
    @include('components.footerLogin')
@endif

<script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
