<!doctype html>
<html lang="{{ app()->getLocale() }}}">
<head>
    @include('layouts.head')
</head>
<body>

<div class="pages">
    @php
        $preheader=[
        '1'=>[
                    'name'=>$_SESSION['name'].' '.$_SESSION['surname'],
                    'link'=>'user/'.$_SESSION['userId'],
            ],
            '2'=>[
                    'name'=>'Мои покупки',
                    'link'=>'myPurchases',
            ],
        ];
    @endphp

    @include('components.header')
    @include('components.preheader',['preheader'=>$preheader])
    @include('components.login')
    @include('components.registration')
    @include('components.recovery')
    @include('components.delete-ad')
    @include('components.popup')
    @include('pages.myPurchases')
</div>
@include('components.footer')


<script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
