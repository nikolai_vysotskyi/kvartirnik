<div class="popup">
    <div class="popup_mheader">
        <div class="menu-button menu-p-button mod_close">
            <div class="top"></div>
            <div class="center"></div>
            <div class="bottom"></div>
        </div>
        <nav class="nav-mobile">
            <a href='#' class="logo">
                @include('svg.logo')
                <p>квартирник</p>
            </a>
        </nav>
    </div>
    <div class="popup_name ">{{isset($_SESSION['name'])?($_SESSION['name'].' '.$_SESSION['surname']):''}}</div>
    <ul class="popup_list">
        <li class="popup_item"><a href="/mySuggestions"><span class="popup_icon">@include('svg.tag')</span>Мои предложения</a></li>
        <li class="popup_item"><a href="/myPurchases"><span class="popup_icon">@include('svg.shield')</span>Мои покупки</a></li>
        <li class="popup_item"><a href="/myOrders"><span class="popup_icon">@include('svg.bell')</span>Мои заказы</a></li>
        <li class="popup_item"><a href="/personalData"><span class="popup_icon">@include('svg.user')</span>Личные данные</a></li>
        <li class="popup_item popup_item-bucket"><a href="/personalData"><span class="popup_icon">@include('svg.bucket')</span>Корзина</a></li>

    </ul>
    <div class="popup_footer"><a href="#">Выйти
            <span class="popup_icon">@include('svg.logout')</span></a></div>
    <section class="footer footer_mobile">
        <div class="footer_bottom">
            <a href="/product"><span class="footer_bottom-el">
                    @include('svg.plus')
                    добавить объявление</span></a>
            <span class="footer_bottom-el">{{isset($_SESSION['name'])?($_SESSION['name'].' '.$_SESSION['surname']):''}}</span>
            <span class="footer_bottom-el">© Квартирник, 2018</span>
        </div>
    </section>
</div>
