<section class="footer">
    <div class="footer_top">
        <h4 class="footer_top-el category_link" category="0">Квартиры</h4>
        <h4 class="footer_top-el category_link" category="1">Дома</h4>
        <h4 class="footer_top-el category_link" category="2">Нежелые помещения</h4>
    </div>
    <div class="footer_bottom">
        <div class="footer_bottom-wrapper">
            <span class="footer_bottom-el">© Квартирник, 2018</span>
            <a class="footer_bottom-el login_link">
                @include('svg.plus')
                <span>добавить объявление</span>
            </a>
            <span class="footer_bottom-el"><a class="user_login login_link"><span>Войти</span></a>&nbsp;/&nbsp;<a class="user_reg"><span>Регистрация</span></a></span>
        </div>
    </div>
</section>

<section class="footer footer_mobile">
    <div class="footer_top">
        <span class="footer_top-el category_link" category="0">Квартиры</span>
        <span class="footer_top-el category_link" category="1">Дома</span>
        <span class="footer_top-el category_link" category="2">Нежелые помещения</span>
    </div>
    <div class="footer_bottom">
                <span class="footer_bottom-el">
                    @include('svg.plus')
                    добавить объявление
                </span>
        <span class="footer_bottom-el"><a class="user_login login_link">Войти</a>&nbsp;/&nbsp;<a class="user_reg"><span>Регистрация</span></a></span>
        <span class="footer_bottom-el">© Квартирник, 2018</span>
    </div>
</section>
