@include('.components.nav-mobile')
<header class="header">
    <div class="header_inner">
        <div class="header_top">
            <nav class="nav-top nav">
                <ul class="nav_list">
                    <li class="nav_item"><a class="category_link" category="0">Квариры</a></li>
                    <li class="nav_item"><a class="category_link" category="1">Дома</a></li>
                    <li class="nav_item"><a class="category_link" category="2">Нежелые помещения</a></li>
                </ul>
                <ul class="nav_list mod_right">
                    <li class="bucket nav_item"><a href="/bucket">
                            @include('svg.bucket')
                            <span>Корзина</span></a></li>
                    <li class="user nav_item header-user_name"><a class="user_link">{{$_SESSION['name'].' '.$_SESSION['surname']}}</a>
                        @include('components.popup')
                    </li>
                </ul>
            </nav>
        </div>
        <div class="header_bottom">
            <nav class="nav-bottom nav">
                <a href='/' class="logo">
                    @include('svg.logo')
                    <p>квартирник</p>
                </a>
                <div class="header_search search">
                    <input class="search_input" type="search" placeholder="Поиск недвижимости">
                    <a href="/search"><input class="search_submit" type="submit" value=""></a>
                </div>
                <a href="/newAdd">
                    <button class="button">+ Подать объявление</button>
                </a>
            </nav>
        </div>
    </div>
</header>

