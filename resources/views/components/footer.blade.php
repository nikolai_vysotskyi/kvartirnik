<section class="footer">
    <div class="footer_top">
        <h4 class="footer_top-el category_link" category="0">Квартиры</h4>
        <h4 class="footer_top-el category_link" category="1">Дома</h4>
        <h4 class="footer_top-el category_link" category="2">Нежелые помещения</h4>
    </div>
    <div class="footer_bottom">
        <div class="footer_bottom-wrapper">
            <span class="footer_bottom-el">© Квартирник, 2018</span>
            <a href="/newAdd"><span class="footer_bottom-el">@include('svg.plus')<span>добавить объявление</span></span></a>
            <span class="footer_bottom-el"><span>{{$_SESSION['name'].' '.$_SESSION['surname']}}</span></span>
        </div>
    </div>
</section>
<section class="footer footer_mobile">
    <div class="footer_top">
        <span class="footer_top-el category_link" category="0">Квартиры</span>
        <span class="footer_top-el category_link" category="1">Дома</span>
        <span class="footer_top-el category_link" category="2">Нежелые помещения</span>
    </div>
    <div class="footer_bottom">
        <a href="/newAdd" class="footer_bottom-el">@include('svg.plus')добавить объявление</a>
        <span class="footer_bottom-el">{{$_SESSION['name'].' '.$_SESSION['surname']}}</span>
        <span class="footer_bottom-el">© Квартирник, 2018</span>
    </div>
</section>
