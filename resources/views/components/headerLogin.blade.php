@include('.components.nav-mobile')
<header class="header">
    <div class="header_inner">
        <div class="header_top">
            <nav class="nav-top nav">
                <ul class="nav_list">
                    <li class="nav_item"><a class="login_link">Квариры</a></li>
                    <li class="nav_item"><a class="login_link">Дома</a></li>
                    <li class="nav_item"><a class="login_link">Нежелые помещения</a></li>
                </ul>
                <ul class="nav_list mod_right">
                    <li class="nav_item user"><a class="user_login login_link">Войти</a>&nbsp;/&nbsp;<a class="user_reg">Регистрация</a></li>
                </ul>
            </nav>
        </div>
        <div class="header_bottom">
            <nav class="nav-bottom nav">
                <a class="logo" href="/">
                    @include('svg.logo')
                    <p>квартирник</p>
                </a>
                <div class="header_search search">
                    <input class="search_input" type="search" placeholder="Поиск недвижимости">
                    <a href="/search"><input class="search_submit" type="submit" value=""></a>
                </div>
                <a class="login_link">
                    <button class="button">+ Подать объявление</button>
                </a>
            </nav>
        </div>
    </div>
</header>
