<section class="pagination">
    <div class="pagination_el pagination_el-active pagination_page-left" value="{{$page-1}}"> @include('svg.left-arrow')</div>
    <div class="pagination_el pagination_el-active pagination_page-first" value="1">1</div>
    <div class="pagination_el pagination_dots">...</div>
    <div class="pagination_el pagination_page-this" value="{{$page}}">{{$page}}</div>
    <div class="pagination_el pagination_dots">...</div>
    <div class="pagination_el pagination_el-active pagination_page-last" value="{{$max_page}}">{{$max_page}}</div>
    <div class="pagination_el pagination_el-active pagination_page-right" value="{{$page+1}}"> @include('svg.right-arrow') </div>
</section>
