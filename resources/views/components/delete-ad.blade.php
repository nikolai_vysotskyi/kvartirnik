<section class="deletead">
    @include('svg.close')
    <div class="deletead_block">
        <h1>Удалить объявление?</h1>
        <div class="deletead_form">
            <p>Вы уверены, что хотите удалить объявление? Данное действие необратимо.</p>
            <div class="deletead_form-buttons">
                <button class="deletead_no">Нет, вернуться</button>
                <button class="deletead_yes">Да, я уверен</button>
            </div>
        </div>
    </div>
</section>
