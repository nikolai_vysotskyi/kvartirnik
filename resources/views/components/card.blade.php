<div class="card">
    <picture class="card_img">
        <source media="(min-width: 650px)" srcset="/images/card/card-prev.png">
        <img src="/images/card/card-mobile.jpg" alt="Flowers">
    </picture>
    <div class="card_content">
        <h3 class="card_title">Квартира дизайнерская для большой семьи</h3>
        <p class="card_description">
            Квартира дизайнерская для большой и хорошей семьи. Квартира дизайнерская для большой семьи. Квартира
            дизайнерская для большой семьи.
        </p>
    </div>
    <div class="card_footer">
        <div class="card_price"><p>5000 @include('svg.bitcoin')</p></div>
        <a href="/product">
            <button class="card_button">Подробнее</button>
        </a>
    </div>
</div>
