<section class="recovery">
    @include('svg.close')
    <div class="recovery_block">
        <h1>Восстановить пароль</h1>
        <span class="recovery_lore">
            <a class="recovery_login" href="#">вход</a> / <a class="recovery_register" href="#">регистрация</a>
        </span>
        <div class="recovery_form">
            <input type="text" placeholder="Введите логин">
            <button>Отправить</button>
        </div>
    </div>
</section>
