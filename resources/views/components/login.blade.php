<section class="login">
    @include('svg.close')
    <div class="login_block">
        <h1>Вход</h1>
        <a class="login_register" href="#">регистрация</a>
        <form class="login_form">
            <input class="log_login" type="text" placeholder="Введите логин">
            <input class="log_pass" type="password" placeholder="Введите пароль">
            <a class="login_password-recovery" href="#">забыли пароль?</a>
            <button class="log_button">Войти</button>
        </form>
    </div>
</section>
