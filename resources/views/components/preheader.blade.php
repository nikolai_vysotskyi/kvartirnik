<section class="preheader">
    <div class="preheader_wrap">
        <a class="preheader_link preheader_link-color" href="/">Главная &nbsp; &rarr; &nbsp;</a>


        @foreach($preheader as $key => $item)
            @if ($item != end($preheader))
                <a class="preheader_link preheader_link-color" href="/{{$item['link']}}"> {{$item['name']}} &nbsp; &rarr; &nbsp;</a>

            @else
                <a class="preheader_link" href="/{{$item['link']}}"> {{$item['name']}} </a>
            @endif
        @endforeach
    </div>
</section>

