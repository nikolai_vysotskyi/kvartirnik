<section class="registration">
 @include('svg.close')
 <div class="registration_block">
     <h1>регистрация</h1>
     <a class="registration_login" href="#">вход</a>
     <div class="registration_form">
         <input class="reg_login" type="text" placeholder="Введите логин">
         <input class="reg_pass1" type="password" placeholder="Введите пароль">
         <input class="reg_pass2" type="password" placeholder="Введите пароль повторно">
         {{ csrf_field() }}
         <button class="reg_button">Зарегистрироваться</button>
     </div>
 </div>
</section>
