@php
    $facilities = [
            'sofa' => 'Диван', 'double-bed' => 'Двухспальная кровать', 'furniture' => 'Мебель',
            'free-wifi' => 'Бесплатный Wi-Fi', 'tv' => 'Телевизор', 'conditioner' => 'Кондиционер',
            'hair-dryer' => 'Фен', 'kettle' => 'Чайник', 'elevator' => 'Лифт', 'boiler' => 'Бойлер', 'security' => 'Охрана',
            'kitchen' => 'Кухня', 'children' => 'Детская комната',
            'parking' => 'Парковка', 'swimming-pool' => 'Бассейн', 'gym' => 'Тренажер',
            'balcony' => 'Балкон', 'repairs' => 'Ремонт', 'soundproofing' => 'Звукоизоляция', 'veranda' => 'Терраса',
            'concierge' => 'Консьерж',
        ]
@endphp


<div class="facilities">
    <h4>Удобства</h4>
    <div class="facilities_list">
        @foreach($facilities as $key => $item)
            <div class="facilities_item">
                <input type="checkbox" id="{{$key}}"><label for="{{$key}}">{{$item}}</label>
            </div>
        @endforeach
    </div>
</div>

<div class="facilities mod_with-icons">
    <div class="facilities_list">
        @foreach($facilities as $key => $item)
            <div class="facilities_item">
                @if(file_exists(resource_path("assets/svg/facilities/$key.svg")))
                    <span class="facilities_icon">@svg("facilities.$key")</span>
                @endif
                <div>{{$item}}</div>
            </div>
        @endforeach
    </div>
</div>