<section class="table">
    <div class="table_wrapper">
        <table class="table_all" cellspacing="0" cellpadding="0">
            <thead>
            <tr class="table_header">
                <th class="table_header-name">Название</th>
                <th class="table_header-name">Стоимость</th>
                <th class="table_header-name"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($element as $key => $item)
                <tr class="table_main">
                    <td class="table_main-text"><a href="/product/{{$product[$key]->id}}">{{ $product[$key]->name }}</a></td>
                    <td class="table_main-text">{{ $product[$key]->price }} @include('svg.bitcoin')</td>
                    <td class="table_main-text table_main-delete" productId="{{ $product[$key]->id }}"><a>Удалить</a></td>
                    <td class="table_main-text table_main-mdelte" productId="{{ $product[$key]->id }}"><a>@include('svg.close')</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</section>
