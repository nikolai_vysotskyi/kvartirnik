<svg class="bitcoin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8.677 12" class="table_main-bitcoin    ">
    <defs>
        <style>
            .bitcoin{
                width: 14px;
                margin-left: 5px;
            }
            .bitcoin .cls-1 {
                fill: #C1C1C1;
            }
        </style>
    </defs>
    <path id="Path_210" data-name="Path 210" class="cls-1" d="M18.92,11.444a1.713,1.713,0,0,0,.95-1.861c-.126-1.311-1.259-1.75-2.689-1.875V5.89H16.073V7.66c-.291,0-.589.006-.884.012V5.89H14.081V7.707c-.24,0-.475.01-.705.01V7.712H11.848V8.893s.818-.015.8,0a.572.572,0,0,1,.637.485v4.98a.391.391,0,0,1-.416.367c.014.013-.806,0-.806,0l-.22,1.321h1.441c.269,0,.532,0,.791.007V17.89h1.106V16.071c.3.006.6.009.885.009V17.89h1.108V16.055c1.862-.107,3.165-.575,3.328-2.323A1.884,1.884,0,0,0,18.92,11.444Zm-3.7-2.472c.625,0,2.59-.2,2.59,1.105,0,1.25-1.964,1.1-2.59,1.1Zm0,5.76V12.3c.751,0,3.1-.216,3.105,1.217C18.32,14.889,15.966,14.732,15.214,14.733Z" transform="translate(-11.848 -5.89)"/>
</svg>
