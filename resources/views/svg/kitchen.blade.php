<svg class="kitchen" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.965 20.5">
  <defs>
    <style>
        .kitchen{
            width: 22px;
        }
      .kitchen .cls-1 {
        fill: #ff625b;
        stroke: #f8f6f6;
        stroke-width: 0.5px;
      }
    </style>
  </defs>
  <g id="coffee-machine" transform="translate(-19.403 0.25)">
    <path id="Path_161" data-name="Path 161" class="cls-1" d="M19.653,0V3.21A2.293,2.293,0,0,0,21.831,5.5V8.419h2.7V9.3H25.7V8.419h2.7V5.5h4.223V16.256H27.711A3.642,3.642,0,0,0,28.5,15.2h.623a1.636,1.636,0,0,0,0-3.272h-.256V11.4h-7.5v2.213a3.613,3.613,0,0,0,1.152,2.647H21.18a1.529,1.529,0,0,0-1.527,1.527V20H38.118V0Zm9.21,13.609V13.1h.256a.464.464,0,0,1,0,.929h-.28A3.64,3.64,0,0,0,28.863,13.609Zm-1.64-6.363H23V5.5h4.22Zm-4.688,5.321h5.156v1.042A2.454,2.454,0,0,1,25.24,16.06h-.254a2.454,2.454,0,0,1-2.451-2.451V12.568Zm14.411,6.261H20.825V17.783a.356.356,0,0,1,.355-.355H36.946Zm0-8.871H35.368v1.172h1.578v.741H35.368v1.172h1.578v.741H35.368v1.172h1.578V16.06H33.79V5.5h3.156Zm0-5.629h-15A1.12,1.12,0,0,1,20.825,3.21V1.172H36.946Z"/>
    <rect id="Rectangle_150" data-name="Rectangle 150" class="cls-1" width="1.172" height="0.752" transform="translate(24.527 10.028)"/>
    <rect id="Rectangle_151" data-name="Rectangle 151" class="cls-1" width="1.182" height="1.182" transform="translate(34.696 2.159)"/>
    <rect id="Rectangle_152" data-name="Rectangle 152" class="cls-1" width="1.182" height="1.182" transform="translate(32.47 2.159)"/>
  </g>
</svg>
