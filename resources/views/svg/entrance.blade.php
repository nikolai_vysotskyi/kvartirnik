<svg class="mainfeatures_characteristic-entrance" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 21.405 31.547">
  <defs>
    <style>
      .mainfeatures_characteristic-entrance .cls-1 {
        fill: #ff625b;
      }
    </style>
  </defs>
  <g id="entrance" transform="translate(-41.361)">
    <path id="Path_184" data-name="Path 184" class="cls-1" d="M62.206,0H41.922a.561.561,0,0,0-.561.561V30.986a.561.561,0,0,0,.561.561H62.206a.561.561,0,0,0,.561-.561V.561A.561.561,0,0,0,62.206,0Zm-.561,30.425H42.482V1.122H61.645Z"/>
    <path id="Path_185" data-name="Path 185" class="cls-1" d="M77.491,172.348H89.053a.561.561,0,0,0,.561-.561v-5.781a.561.561,0,0,0-.561-.561H77.491a.561.561,0,0,0-.561.561v5.781A.56.56,0,0,0,77.491,172.348Zm.561-5.781H88.493v4.66H78.051Z" transform="translate(-31.208 -145.162)"/>
    <path id="Path_186" data-name="Path 186" class="cls-1" d="M77.491,48.251H89.053a.561.561,0,0,0,.561-.561V36.128a.561.561,0,0,0-.561-.561H77.491a.561.561,0,0,0-.561.561V47.69A.56.56,0,0,0,77.491,48.251Zm.561-11.562H88.493V47.129H78.051Z" transform="translate(-31.208 -31.207)"/>
    <path id="Path_187" data-name="Path 187" class="cls-1" d="M175.734,138.991a1.851,1.851,0,1,0,1.851-1.851A1.853,1.853,0,0,0,175.734,138.991Zm1.851-.916a.916.916,0,1,1-.916.916A.917.917,0,0,1,177.585,138.075Z" transform="translate(-117.899 -120.327)"/>
  </g>
</svg>
