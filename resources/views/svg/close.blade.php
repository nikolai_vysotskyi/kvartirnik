<svg class="close_icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 29.699 29.699">
    <defs>
        <style>
           .close_icon .cls-1 {
                fill: none;
                stroke: #747474;
                stroke-width: 2px;
            }
        </style>
    </defs>
    <g id="Group_69" data-name="Group 69" transform="translate(862.672 -594.677) rotate(135)">
        <line id="Line_31" data-name="Line 31" class="cls-1" x2="40" transform="translate(1010.5 168.5)"/>
        <line id="Line_32" data-name="Line 32" class="cls-1" y2="40" transform="translate(1030.5 148.5)"/>
    </g>
</svg>
