<svg class="pagination_arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7.702 12">
    <defs>
        <style>
            .pagination_arrow .cls-1 {
                fill: #323232;
            }
        </style>
    </defs>
    <g id="left-arrow-angle" transform="translate(-35.709)">
        <path id="Path_3" data-name="Path 3" class="cls-1" d="M41.709,0l-5,5-1,1,6,6,1.7-1.7L41.738,8.625,39.113,6l4.3-4.3Z"/>
    </g>
</svg>

