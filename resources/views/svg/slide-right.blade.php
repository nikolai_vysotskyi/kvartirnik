<svg  class="recommend_slide-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.658 14.651">
    <defs>
        <style>
            .recommend_slide-right .cls-1 {
                fill: none;
                stroke: #323232;
            }
        </style>
    </defs>
    <g id="Group_525" data-name="Group 525" transform="translate(-1030 -1482.666)">
        <line id="Line_10" data-name="Line 10" class="cls-1" x2="50" transform="translate(1030 1489.5)"/>
        <path id="Path_26" data-name="Path 26" class="cls-1" d="M-331,1480l6,6.666-6,7.334" transform="translate(1405 3)"/>
    </g>
</svg>
