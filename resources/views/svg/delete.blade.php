<svg class="mySuggestions_delete-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.813 19.813">
  <defs>
    <style>

      .mySuggestions_delete-icon .cls-1, .mySuggestions_delete-icon .cls-3 {
        fill: none;
      }

      .mySuggestions_delete-icon .cls-1 {
        stroke: #fff;
        stroke-width: 2px;
      }

      .mySuggestions_delete-icon .cls-2 {
        stroke: none;
      }
      .mySuggestions_delete-icon .cls-1, .mySuggestions_delete-icon .cls-4 {
        fill: none;
      }

      .mySuggestions_delete-icon .cls-1 {
        stroke: #fff;
        stroke-width: 2px;
      }

      .mySuggestions_delete-icon .cls-2 {
        fill: #fff;
      }

      .mySuggestions_delete-icon .cls-3 {
        stroke: none;
      }
    </style>
  </defs>
  <g id="Group_51" data-name="Group 51" transform="translate(-555 -505)">
    <g id="Ellipse_11" data-name="Ellipse 11" class="cls-1" transform="translate(555 505)">
      <circle class="cls-3" cx="9.907" cy="9.907" r="9.907"/>
      <circle class="cls-4" cx="9.907" cy="9.907" r="8.907"/>
    </g>
    <g id="Group_50" data-name="Group 50" transform="translate(561.439 511.439)">
      <line id="Line_20" data-name="Line 20" class="cls-1" x2="9.251" transform="translate(0 0) rotate(45)"/>
      <line id="Line_21" data-name="Line 21" class="cls-1" y2="9.251" transform="translate(6.541) rotate(45)"/>
    </g>
  </g>
</svg>
