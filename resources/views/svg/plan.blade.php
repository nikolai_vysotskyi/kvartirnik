<svg class="mainfeatures_characteristic-plan" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.4 20.4">
    <defs>
        <style>
            .mainfeatures_characteristic-plan .cls-1 {
                fill: #ff625b;
                stroke: #ff625b;
                stroke-width: 0.4px;
            }
        </style>
    </defs>
    <path id="plan" class="cls-1" d="M19.6,0H.4A.4.4,0,0,0,0,.4V19.6a.4.4,0,0,0,.4.4H12.11a.4.4,0,0,0,0-.8H.8V9.83H6.41v3.853a.4.4,0,1,0,.8,0V5.268a.4.4,0,1,0-.8,0V9.026H.8V.8h10.9V5.835a.4.4,0,0,0,.4.4h4.25a.4.4,0,0,0,0-.8H12.512V.8H19.2V13.28H12.11a.4.4,0,0,0,0,.8H19.2V19.2H16.36a.4.4,0,0,0,0,.8H19.6a.4.4,0,0,0,.4-.4V.4A.4.4,0,0,0,19.6,0Z" transform="translate(0.199 0.2)"/>
</svg>
