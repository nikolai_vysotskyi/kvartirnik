<svg class="mySuggestions_edit-icon" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
  <defs>
    <style>
      .mySuggestions_edit-icon .cls-1, .mySuggestions_edit-icon .cls-4 {
        fill: none;
      }

      .mySuggestions_edit-icon .cls-1 {
        stroke: #fff;
        stroke-width: 2px;
      }

      .mySuggestions_edit-icon .cls-2 {
        fill: #fff;
      }

      .mySuggestions_edit-icon .cls-3 {
        stroke: none;
      }
    </style>
  </defs>
  <g id="Group_53" data-name="Group 53" transform="translate(-640 -505)">
    <g id="Ellipse_12" data-name="Ellipse 12" class="cls-1" transform="translate(640 505)">
      <circle class="cls-3" cx="10" cy="10" r="10"/>
      <circle class="cls-4" cx="10" cy="10" r="9"/>
    </g>
    <g id="Group_52" data-name="Group 52" transform="translate(644 513.5)">
      <circle id="Ellipse_13" data-name="Ellipse 13" class="cls-2" cx="1.5" cy="1.5" r="1.5" transform="translate(0)"/>
      <circle id="Ellipse_14" data-name="Ellipse 14" class="cls-2" cx="1.5" cy="1.5" r="1.5" transform="translate(4.5)"/>
      <circle id="Ellipse_15" data-name="Ellipse 15" class="cls-2" cx="1.5" cy="1.5" r="1.5" transform="translate(9)"/>
    </g>
  </g>
</svg>
