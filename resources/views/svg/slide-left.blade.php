<svg class="recommend_slide-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.659 14.651">
    <defs>
        <style>
            .recommend_slide-left .cls-1 {
                fill: none;
                stroke: #323232;
            }
        </style>
    </defs>
    <g id="Group_525" data-name="Group 525" transform="translate(-759.341 -1481.983)">
        <line id="Line_9" data-name="Line 9" class="cls-1" x2="50" transform="translate(760 1489.5)"/>
        <path id="Path_203" data-name="Path 203" class="cls-1" d="M-331,1480l5.2,5.777.8.889-6,7.334" transform="translate(435 2976.3) rotate(180)"/>
    </g>
</svg>
