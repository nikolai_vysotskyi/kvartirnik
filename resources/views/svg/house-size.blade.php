<svg class="mainfeatures_characteristic-housesize" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.437 24.4">
  <defs>
    <style>
      .mainfeatures_characteristic-housesize .cls-1 {
        fill: #ff625b;
        stroke: #ff625b;
        stroke-width: 0.4px;
      }
    </style>
  </defs>
  <g id="house-size" transform="translate(0.199 0.2)">
    <path id="Path_177" data-name="Path 177" class="cls-1" d="M90.512,103.3a.411.411,0,0,0-.411.411v6.243a.411.411,0,0,0,.411.411H103a.411.411,0,0,0,.411-.411V103.71a.411.411,0,1,0-.823,0v5.832H99.248V103.71a.411.411,0,0,0-.411-.411H94.675a.411.411,0,0,0-.411.411v5.832H90.924V103.71A.412.412,0,0,0,90.512,103.3Zm4.573.823h3.339v5.42H95.086Z" transform="translate(-81.995 -94.007)"/>
    <path id="Path_178" data-name="Path 178" class="cls-1" d="M62.055,14.685l8.538-8.538,8.538,8.538a.411.411,0,0,0,.582-.582L75.419,9.809V5.565a.411.411,0,1,0-.823,0V8.987L70.884,5.275a.412.412,0,0,0-.582,0L61.473,14.1a.411.411,0,0,0,.582.582Z" transform="translate(-55.833 -4.69)"/>
    <path id="Path_179" data-name="Path 179" class="cls-1" d="M79.421,210.458a.411.411,0,0,0-.411.411v1.711H62.175v-1.711a.411.411,0,1,0-.823,0v4.245a.411.411,0,1,0,.823,0V213.4H79.01v1.711a.411.411,0,1,0,.823,0v-4.245A.411.411,0,0,0,79.421,210.458Z" transform="translate(-55.832 -191.526)"/>
    <path id="Path_180" data-name="Path 180" class="cls-1" d="M4.658.823a.411.411,0,1,0,0-.823H.412a.411.411,0,0,0,0,.823H2.124V17.658H.412a.411.411,0,0,0,0,.823H4.658a.411.411,0,0,0,0-.823H2.946V.823Z"/>
  </g>
</svg>
