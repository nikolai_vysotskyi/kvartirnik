<svg class="landlord_characteristic-home" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 21.229 19.959">
    <defs>
        <style>
            .landlord_characteristic-home .cls-1 {
                fill: none;
            }

            .landlord_characteristic-home .cls-2 {
                clip-path: url(#clip-path);
            }

            .landlord_characteristic-home .cls-3 {
                fill: #ff625b;
            }
        </style>
        <clipPath id="clip-path">
            <rect class="cls-1" width="21.229" height="19.959"/>
        </clipPath>
    </defs>
    <g id="house-outline" class="cls-2" transform="translate(0)">
        <g id="house-outline-2" data-name="house-outline" transform="translate(1.322 -17.057)">
            <path id="Path_170" data-name="Path 170" class="cls-3" d="M98.44,300.251a.6.6,0,0,0-.612.612v8.363H93.49v-3.415a.6.6,0,0,0-.59-.6H89.763a.619.619,0,0,0-.623.6v3.415H84.812v-8.086a.6.6,0,0,0-1.2,0v8.7a.6.6,0,0,0,.59.591h5.562a.6.6,0,0,0,.59-.591v-3.415h1.936v3.415a.6.6,0,0,0,.612.591h5.54a.6.6,0,0,0,.59-.591v-8.976A.6.6,0,0,0,98.44,300.251Z" transform="translate(-82.033 -273.414)"/>
            <path id="Path_171" data-name="Path 171" class="cls-3" d="M21.055,28.557l-4.828-4.849V20.4a.611.611,0,0,0-.623-.61.6.6,0,0,0-.59.61V22.5L11.032,18.49a.585.585,0,0,0-.412-.19.6.6,0,0,0-.434.19l-10,10a.573.573,0,0,0,0,.845.573.573,0,0,0,.845,0l9.589-9.567,9.567,9.657a.591.591,0,0,0,.434.157.672.672,0,0,0,.434-.157A.631.631,0,0,0,21.055,28.557Z" transform="translate(-1.322 -1.243)"/>
        </g>
    </g>
</svg>
