<svg class="down_arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13.414 8.121">
  <defs>
    <style>
      .down_arrow .cls-1 {
        fill: none;
        stroke: #323232;
        stroke-width: 2px;
      }
    </style>
  </defs>
  <path id="Path_6" data-name="Path 6" class="cls-1" d="M7477,280l6,6,6-6" transform="translate(-7476.293 -279.293)"/>
</svg>
