<svg class="pagination_arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7 12">
    <defs>
        <style>
            .pagination_arrow .cls-1 {
                fill: #323232;
            }
        </style>
    </defs>
    <g id="left-arrow-angle" transform="translate(42.709 12) rotate(180)">
        <path id="Path_3" data-name="Path 3" class="cls-1" d="M41.162,0,35.709,6l5.453,6,1.547-1.7L38.8,6l3.906-4.3Z"/>
    </g>
</svg>

