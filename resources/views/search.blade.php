<!doctype html>
<html lang="{{ app()->getLocale() }}}">
<head>
    @include('layouts.head')
</head>
<body>

<div class="pages">
    @php
        $preheader=[
            '1'=>[
                'name'=>' Поиск недвижимости',
                'link'=>'search',
            ],
        ];
    @endphp
    @if(isset($_SESSION['username']))
        <div class="pages">
            @include('components.header')
            @include('components.preheader',['preheader'=>$preheader])
            @include('pages.search')

            @include('components.delete-ad')
            @include('components.popup')
        </div>
        @include('components.footer')
    @else
        <div class="pages">
            @include('components.headerLogin')
            @include('components.preheader',['preheader'=>$preheader])
            @include('pages.search')

            @include('components.login')
            @include('components.registration')
            @include('components.recovery')
            @include('components.popup')
        </div>
        @include('components.footerLogin')
    @endif
    <script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
