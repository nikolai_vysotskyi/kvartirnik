<section class="myPurchases">
    <div class="myPurchases_name">
        <h1>МИХАИЛ ПАВЛОВ</h1>
        <a>Мои покупки</a>
    </div>
    @foreach($purchases as $key => $purchase)
        <section class="table">
            <div class="table_wrapper">
                <table class="table_all" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr class="table_header">
                        <th class="table_header-name">№ {{$purchase->id}}</th>
                        <th class="table_header-name">{{date("m.d.Y",strtotime($purchase->updated_at))}}</th>
                        <th class="table_header-name">{{$purchase->count}} товара
                            на {{$purchase->price}} @include('svg.bitcoin')</th>
                        <th class="table_header-name"><span>Подробности</span> @include('svg.down-arrow')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products[$key] as $product)
                        <tr class="table_main">
                            <td class="table_main-text"><a href="/product/{{$product['id']}}">{{$product['name']}}</a></td>
                            <td class="table_main-text"></td>
                            <td class="table_main-text"></td>
                            <td class="table_main-text">{{$product['price']}} @include('svg.bitcoin')</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </section>
    @endforeach
</section>
