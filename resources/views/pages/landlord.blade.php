<section class="landlord">
    <div class="landlord_name">
        <h1>{{$user->name.' '.$user->surname}}</h1>
    </div>
    <div class="landlord_characteristics">
        <div class="landlord_characteristic">
            <div class="landlord_characteristic-icon">
                <div class="landlord_characteristic-iconinner">
                    I
                </div>
            </div>
            <div class="landlord_characteristic-text">
                1-комната:
                <div>22</div>
            </div>
        </div>
        <div class="landlord_characteristic">
            <div class="landlord_characteristic-icon">
                <div class="landlord_characteristic-iconinner">
                    II
                </div>
            </div>
            <div class="landlord_characteristic-text">
                2-комнаты:
                <div>78</div>
            </div>
        </div>
        <div class="landlord_characteristic">
            <div class="landlord_characteristic-icon">
                <div class="landlord_characteristic-iconinner">
                    III
                </div>
            </div>
            <div class="landlord_characteristic-text">
                3-комнаты:
                <div>12</div>
            </div>
        </div>
        <div class="landlord_characteristic">
            <div class="landlord_characteristic-icon">
                <div class="landlord_characteristic-iconinner">
                    @include('svg.house-outline')
                </div>
            </div>
            <div class="landlord_characteristic-text">
                Дома:
                <div>2</div>
            </div>
        </div>
    </div>
    <div class="landlord_territory title-4">
        <div class="landlord_territory-1">
            <div>Всего:
                @if($countProducts>=5):
                {{$countProducts}} предложений
                @else:
                {{$countProducts}} предложения
                @endif
            </div>
            <div>{{$user->email}}</div>
            <div>{{$user->phone}}</div>
        </div>
    </div>
</section>
<section class="landlordn" useriD="{{$user->id}}">
    <div class="landlordn_name">
        <h1>ВСЕ ПРЕДЛОЖЕНИЯ</h1>
    </div>
    @if($countProducts>0)
        <div class="landlordn_cards">
            @foreach($products as $el)
                <div class="card">
                    <picture class="card_img">
                        @if($photos[$el->id]['img'])
                            <source media="(min-width: 650px)" srcset="{{ $photos[$el->id]['img'] }}">
                            <img src="{{ $photos[$el->id]['img'] }}" alt="Flowers">
                        @else
                            <source media="(min-width: 650px)" srcset="/storage/non.png">
                            <img src="/storage/non.png" alt="Flowers">
                        @endif
                    </picture>
                    <div class="card_content">
                        <h3 class="card_title">{{$el['name']}}</h3>
                        <p class="card_description">{{$el['description']}}</p>
                    </div>
                    <div class="card_footer">
                        <div class="card_price"><p>{{$el['price']}} @include('svg.bitcoin')</p></div>
                        <a href="/product/{{$el['id']}}">
                            <button class="card_button">Подробнее</button>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
        @if($max_page!=1)
            @include('components.pagination', ['page'=> $_SESSION['page-user'.$user->id],'max_page'=>$max_page])
        @endif
    @else
        <section class="landlordf">
            У данного пользователя еще нет объявлений, но Вы можете связаться с ним.
        </section>
    @endif
</section>
