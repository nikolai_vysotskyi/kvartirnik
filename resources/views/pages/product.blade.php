<section class="mainfeatures" productId="{{$product[0]->id}}">
    <div class="mainfeatures_name">
        <h1>{{ $product[0]->name }}</h1>
    </div>
    <div class="mainfeatures_characteristics">
        <div class="mainfeatures_characteristic">
            <div class="mainfeatures_characteristic-icon">
                <div class="mainfeatures_characteristic-iconinner">
                    @include('svg.plan')
                </div>
            </div>
            <div class="mainfeatures_characteristic-text">
                Комнат:
                <div>{{$countRooms[$product[0]->countRooms-1]->number}}</div>
            </div>
        </div>
        <div class="mainfeatures_characteristic">
            <div class="mainfeatures_characteristic-icon">
                <div class="mainfeatures_characteristic-iconinner">
                    @include('svg.house-size')
                </div>
            </div>
            <div class="mainfeatures_characteristic-text">
                Общая площадь:
                <div>{{$product[0]->totalArea}}</div>
            </div>
        </div>
        <div class="mainfeatures_characteristic">
            <div class="mainfeatures_characteristic-icon">
                <div class="mainfeatures_characteristic-iconinner">
                    @include('svg.square')
                </div>
            </div>
            <div class="mainfeatures_characteristic-text">
                Жилая площадь:
                <div>{{$product[0]->livingArea}}</div>
            </div>
        </div>
        <div class="mainfeatures_characteristic">
            <div class="mainfeatures_characteristic-icon">
                <div class="mainfeatures_characteristic-iconinner">
                    @include('svg.kitchen')
                </div>
            </div>
            <div class="mainfeatures_characteristic-text">
                Площадь кухни:
                <div>{{$product[0]->kitchenArea}}</div>
            </div>
        </div>
        <div class="mainfeatures_characteristic">
            <div class="mainfeatures_characteristic-icon">
                <div class="mainfeatures_characteristic-iconinner">
                    @include('svg.building')
                </div>
            </div>
            <div class="mainfeatures_characteristic-text">
                Этажность:
                <div>{{$floors[$product[0]->floors]->number}}</div>
            </div>
        </div>
        <div class="mainfeatures_characteristic">
            <div class="mainfeatures_characteristic-icon">
                <div class="mainfeatures_characteristic-iconinner">
                    @include('svg.entrance')
                </div>
            </div>
            <div class="mainfeatures_characteristic-text">
                Этаж:
                <div>{{$product[0]->floor + 1}}</div>
            </div>
        </div>
    </div>
    <div class="mainfeatures_territory title-4">
        <div class="mainfeatures_territory-1">
            <div>{{$locality[$product[0]->locality]->name}}</div>
            <div>{{$district[$product[0]->district]->name}}</div>
            <div>Фото</div>
        </div>
    </div>
    <div class="mainfeatures_realtors">
        <div class="mainfeatures_realtors-wrap">
            <div class="mainfeatures_realtor">
                <div class="title-2">{{$seller[0]->name}} {{$seller[0]->surname}}</div>
                <a href="/user/{{$seller[0]->id}}">показать все объявления</a>
            </div>
            <div class="mainfeatures_price">
                <div class="card_price"><p>{{$product[0]->price}}</p>@include('svg.bitcoin')</div>
                @isset($_SESSION['userId'])
                    @if($product[0]->userId != $_SESSION['userId'] && $product[0]->customerId == 0)
                        <button class="card_button">Приобрести</button>
                    @endif
                @endif
            </div>
        </div>
    </div>
</section>
<section class="aboutflat">
    <div class="aboutflat_block-l">
        <div class="aboutflat_name">
            <h1>ЧТО В КВАРТИРЕ</h1>
        </div>
        <p>{{$product[0]->description}}</p>
    </div>
    <div class="aboutflat_block-r">
        <div class="aboutflat_blockr-img">
            <ul class="aboutflat_slidewrapper" style="width:{{count($photos)*100}}%;">
                @foreach($photos as $photo)
                    <li style="width: {{100/count($photos)}}%;"><img src="{{$photo['img']}}" alt=""></li>
                @endforeach
            </ul>
            <ul class="aboutflat_nav-btns">
                @foreach($photos as $photo)
                    <li class="slide-nav-btn"></li>
                @endforeach
            </ul>
        </div>

        @php
            $facs = explode(' ', $product[0]->facilities);
        @endphp
        <div class="facilities mod_with-icons">
            <div class="facilities_list">
                @if($product[0]->facilities)
                    @foreach($facs as $item)
                        <div class="facilities_item">
                            @if(file_exists(resource_path("views/svg/".$facilities[$item]->svg.".blade.php")))
                                <span class="facilities_icon">@include("svg.".$facilities[$item]->svg)</span>
                            @endif
                            <div>{{$facilities[$item]->title}}</div>
                        </div>
                    @endforeach
                @else
                    <div class="facilities_item">Без удобств</div>
                @endif
            </div>
        </div>
    </div>
</section>

<section class="comments">
    @if(count($comments))
        <div class="comments_name">
            <h1>ЧТО ГОВОРЯТ</h1>
        </div>
        @foreach($comments as $comment)
            <div class="comments_feedback">
                <div class="comments_feedback-about">
                    <div class="comments_realtor">
                        <div class="comments_realtor-name title-2">{{$comment->userName->name.' '.$comment->userName->surname}}</div>
                        <a class="title-4" href="/user/{{$comment->userId}}">показать все объявления</a>
                    </div>
                    @php
                        $arr = ['январz','февралz', 'марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
                        $month = date('n',strtotime($comment->created_at))-1;
                        $year = date("Y",strtotime($comment->created_at));
                        $day = date("j",strtotime($comment->created_at));
                    @endphp

                    <a class="comments_date">{{ $day.' '.$arr[$month].' '.$year }}</a>
                </div>
                <p>{{$comment->text}}</p>
            </div>
        @endforeach
    @else
        <div class="comments_name">
            <h1>ОСТАВЬТЕ ОТЗЫВ</h1>
        </div>
    @endif

    <div class="comments_textarea">
        <div class="comments_new-name title-1">Написать сообщение</div>
        <textarea class="comments_new-feedback title-4" name="" id="" cols="30" rows="5"
                  placeholder="Сообщение"></textarea>
    </div>
    <div class="comments_new-feeddata">
        @if(!isset($_SESSION['username']))
            <input class="comments_new-feedname" type="text" placeholder="Ваше имя">
            <input class="comments_new-feedmail" type="text" placeholder="email">
        @endif
        <button class="comments_new-button">Отправить</button>
    </div>
</section>