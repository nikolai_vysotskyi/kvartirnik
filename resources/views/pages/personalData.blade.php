<section class="personalData">
    <div class="personalData_name">
        <h1>МИХАИЛ ПАВЛОВ</h1>
        <a>Личные данные</a>
    </div>
    <div class="personalData_wrapper">
        <div class="personalData_block">
            <div class="personalData_blocks">
                <div class="personalData_inputs">
                    <h4 class="personalData_iname">Имя</h4>
                    <input class="personalData_input personalData_input-name" type="text" value="{{$user['name']}}">
                </div>
                <div class="personalData_inputs">
                    <h4 class="personalData_iname">Фамилия</h4>
                    <input class="personalData_input personalData_input-surname" type="text" value="{{$user['surname']}}">
                </div>
                <div class="personalData_inputs">
                    <h4 class="personalData_iname">Телефон</h4>
                    <input class="personalData_input personalData_input-phone" type="tel" value="{{$user['phone']}}">
                </div>
                <div class="personalData_inputs">
                    <h4 class="personalData_iname">Email</h4>
                    <input class="personalData_input personalData_input-email" type="email" value="{{$user['email']}}">
                </div>
            </div>
            <button class="personalData_button">Сохранить изменения</button>
        </div>
    </div>
</section>
