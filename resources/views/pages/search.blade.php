<section class="search">
    <div class="search_name">
        <h1>ПОИСК НЕДВИЖИМОСТИ</h1>
    </div>

    <div class="search_block">
        <div class="search_block-1">
            <div class="search_form">
                <h4 class="search_label">
                    Ключевое слово
                </h4>
                <input class="search_keyword search_keywords" type="text" value="{{$_SESSION['keyword']}}">
            </div>

            <div class="search_form">
                <h4 class="search_label">
                    Местоположение
                </h4>
                <input class="search_keyword search_locality" type="text" value="{{$_SESSION['locality_name']}}">
            </div>

            <div class="search_form">
                <h4 class="search_label">
                    Категория
                </h4>
                <div class="search_keyword search_category search_keyword-blselect">
                    <div class="search_keyword-bg"></div>
                    <div class="search_keyword-block">
                        @include('svg.down-arrow')
                    </div>
                    <select class="search_keyword-select">
                        @if($_SESSION['category'] === '%')
                            <option value="%" class="search_keyword-option"></option>
                            @foreach($category as $cat)
                                <option value="{{$cat->id}}" class="search_keyword-option">{{$cat->name}}</option>
                            @endforeach
                        @else
                            <option value="%" class="search_keyword-option">Все</option>
                            @foreach($category as $cat)
                                <option value="{{$cat->id}}"
                                        class="search_keyword-option" {{(int)$_SESSION['category']+1 == $cat->id ? 'selected':''}}>{{$cat->name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>

        <div class="search_block-2">
            <div class="search_form">
                <h4 class="search_label">
                    Кол-во комнат
                </h4>
                <div class="search_block-b">
                    <div class="search_blocks2 search_blocks2-1">
                        <p class="search_block2-label title-4">от</p>
                        <input class="search_block2-input search_countRooms1" type="text"
                               value="{{$_SESSION['countRooms1']}}">
                    </div>
                    <div class="search_blocks2 search_blocks2-2">
                        <p class="search_block2-label title-4">до</p>
                        <input class="search_block2-input search_countRooms2" type="text"
                               value="{{$_SESSION['countRooms2']}}">
                    </div>
                </div>
            </div>

            <div class="search_form">
                <h4 class="search_label">
                    Этаж
                </h4>
                <div class="search_block-b">
                    <div class="search_blocks2 search_blocks2-1">
                        <p class="search_block2-label title-4">от</p>
                        <input class="search_block2-input search_floor1" type="text" value="{{$_SESSION['floor1']}}">
                    </div>
                    <div class="search_blocks2 search_blocks2-2">
                        <p class="search_block2-label title-4">до</p>
                        <input class="search_block2-input search_floor2" type="text" value="{{$_SESSION['floor2']}}">
                    </div>
                </div>
            </div>

            <div class="search_form">
                <h4 class="search_label">
                    Цена
                </h4>
                <div class="search_block-b">
                    <div class="search_blocks2 search_blocks2-1">
                        <p class="search_block2-label title-4">от</p>
                        <input class="search_block2-input search_price1" type="text" value="{{$_SESSION['price1']}}">
                    </div>
                    <div class="search_blocks2 search_blocks2-2">
                        <p class="search_block2-label title-4">до</p>
                        <input class="search_block2-input search_price2" type="text" value="{{$_SESSION['price2']}}">
                    </div>
                </div>
            </div>
        </div>

        <button class="search_button">Поиск</button>
    </div>

    <div class="search_cards">

        @foreach($products as $el)
            <div class="card">
                <picture class="card_img">
                    @if($photos[$el->id]['img'])
                        <source media="(min-width: 650px)" srcset="{{ $photos[$el->id]['img'] }}">
                        <img src="{{$photos[$el->id]['img']}}" alt="Flowers">
                    @else
                        <source media="(min-width: 650px)" srcset="/storage/non.png">
                        <img src="/storage/non.png" alt="Flowers">
                    @endif
                </picture>
                <div class="card_content">
                    <h3 class="card_title">{{$el['name']}}</h3>
                    <p class="card_description">{{$el['description']}}</p>
                </div>
                <div class="card_footer">
                    <div class="card_price"><p>{{$el['price']}} @include('svg.bitcoin')</p></div>
                    <a href="/product/{{$el['id']}}">
                        <button class="card_button">Подробнее</button>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
        @include('components.pagination', ['page'=>$_SESSION['search-page'],'max_page'=>$products->lastPage()])
</section>
