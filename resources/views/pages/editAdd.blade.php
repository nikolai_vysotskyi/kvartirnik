<section class="editAdd" productId="{{$productId}}">
    <div class="editAdd_name">
        <h1>{{$add->name}}</h1>
        <a>Редактор объявления</a>
    </div>
    <div class="editAdd_wrapper">
        <div class="editAdd_left">
            <h4>Название</h4>
            <input class="editAdd_left-name" type="text" value="{{$add->name}}">

            <h4>Описание</h4>
            <textarea class="editAdd_left-message" name="" id="" cols="30" rows="10">{{$add->description}}</textarea>

            <h4>Фотографии</h4>
            <div class="editAdd_left-ph">
                <div class="editAdd_left-photo">
                    @php
                        $Aphotos = [
                            '0' => 'file',
                            '1' => 'file',
                            '2' => 'file',
                            '3' => 'file',
                            '4' => 'file',
                            '5' => 'file',
                            '6' => 'file',
                            '7' => 'file',
                        ]
                    @endphp

                    @foreach($Aphotos as $key => $item)
                        @php $a = 0; @endphp
                        @foreach($photos as $item)
                            @if($key == $item['photoNumber'])
                                @php $a = 1; @endphp
                                <div class="editAdd_left-file">
                                    <div class="editAdd_left-img Add_left-checkphoto"
                                         style="background-image: url({{$item['img']}})"></div>
                                    @if ($key==0)
                                        <div CLASS="editAdd_left-mainphoto">Основное фото</div>
                                    @endif
                                    <input class="editAdd_left-fileb" type="file" name="file" nphoto="{{$key}}"
                                           multiple>
                                </div>
                            @endif
                        @endforeach
                        @if($a == 0)
                            <div class="editAdd_left-file">
                                <div class="editAdd_left-img"></div>
                                @if ($key==0)
                                    <div CLASS="editAdd_left-mainphoto">Основное фото</div>
                                @endif
                                <input class="editAdd_left-fileb" type="file" name="file" nphoto="{{$key}}"
                                       multiple>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="editAdd_right">
            <div class="editAdd_specifications">
                <div class="editAdd_specification">
                    <h4>Категория</h4>
                    <div class="editAdd_specifications-blselect">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="editAdd_specifications-select editAdd_category">
                            @foreach($category as $item)
                                <option value="{{$item['id']}}"
                                        class="editAdd_specifications-option" {{$add->category + 1 == $item['id'] ? 'selected="selected"' : ''}}>{{$item['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="editAdd_specification">
                    <h4>Тип</h4>
                    <div class="editAdd_specifications-blselect">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="editAdd_specifications-select editAdd_type">
                            @foreach($type as $item)
                                <option value="{{$item['id']}}"
                                        class="editAdd_specifications-option" {{$add->type + 1 == $item['id'] ? 'selected="selected"' : ''}}>{{$item['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="editAdd_specification">
                    <h4>Цена</h4>
                    <div class="editAdd_specifications-blselect editAdd_specifications-price">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            <div class="bitcoin">@include('svg.bitcoin')</div>
                        </div>
                        <input class="editAdd_specifications-select editAdd_price" value="1200">
                    </div>
                </div>

                <div class="editAdd_specification">
                    <h4>Населенный пункт</h4>
                    <div class="editAdd_specifications-blselect editAdd_specifications-2">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="editAdd_specifications-select editAdd_specifications-2 editAdd_city">
                            @foreach($locality as $item)
                                <option value="{{$item['id']}}"
                                        class="editAdd_specifications-option editAdd_city-select" {{$add->locality + 1 == $item['id'] ? 'selected="selected"' : ''}}>{{$item['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="editAdd_specification">
                    <h4>Район</h4>
                    <div class="editAdd_specifications-blselect editAdd_specifications-2">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="editAdd_specifications-select editAdd_specifications-2 editAdd_district">
                            @foreach($district as $item)
                                <option value="{{$item['id']}}"
                                        class="editAdd_specifications-option" {{$add->district + 1 == $item['id'] ? 'selected="selected"' : ''}}>{{$item['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="editAdd_specification editAdd_specification-3">
                    <h4>Кол-во комнат</h4>
                    <div class="editAdd_specifications-blselect editAdd_specifications-3">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="editAdd_specifications-select editAdd_specifications-3 editAdd_countRooms">
                            @foreach($countRooms as $item)
                                <option value="{{$item['id']}}"
                                        class="editAdd_specifications-option" {{$add->countRooms + 1 == $item['id'] ? 'selected="selected"' : ''}}>{{$item['number']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="editAdd_specification editAdd_specification-3">
                    <h4>Этажность</h4>
                    <div class="editAdd_specifications-blselect editAdd_specifications-3">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="editAdd_specifications-select editAdd_specifications-3 editAdd_floors">
                            @foreach($floors as $item)
                                <option value="{{$item['id']}}"
                                        class="editAdd_specifications-option" {{$add->floors + 1 == $item['id'] ? 'selected="selected"' : ''}}>{{$item['number']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="editAdd_specification editAdd_specification-3">
                    <h4>Этаж</h4>
                    <div class="editAdd_specifications-blselect editAdd_specifications-3">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="editAdd_specifications-select editAdd_specifications-3 editAdd_floor">
                            @for($i = 1 ; $i<=$floors[$add->floors]->number; $i++)
                                <option value="{{$i}}" class="editAdd_specifications-option">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                </div>

                <div class="editAdd_specification editAdd_specification-3">
                    <h4>Общая площадь</h4>
                    <div class="editAdd_specifications-blselect editAdd_specifications-3">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            <div class="down_arrow">м<sup>2</sup></div>
                        </div>

                        <input class="editAdd_specifications-select editAdd_specifications-3 editAdd_totalArea" placeholder="0"
                               value="258">
                    </div>
                </div>
                <div class="editAdd_specification editAdd_specification-3">
                    <h4>Жилая площадь</h4>
                    <div class="editAdd_specifications-blselect editAdd_specifications-3">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            <div class="down_arrow">м<sup>2</sup></div>
                        </div>

                        <input class="editAdd_specifications-select editAdd_specifications-3 editAdd_livingArea" placeholder="0"
                               value="258">
                    </div>
                </div>
                <div class="editAdd_specification editAdd_specification-3">
                    <h4>Площадь кухни</h4>
                    <div class="editAdd_specifications-blselect editAdd_specifications-3">
                        <div class="editAdd_specifications-bg"></div>
                        <div class="editAdd_specifications-block">
                            <div class="down_arrow">м<sup>2</sup></div>
                        </div>

                        <input class="editAdd_specifications-select editAdd_specifications-3 editAdd_kitchenArea" placeholder="0"
                               value="258">
                    </div>
                </div>
            </div>
            <div class="facilities">
                <h4>Удобства</h4>
                <div class="facilities_list">
                    @php
                        $facs = explode(' ', $add->facilities);
                        $facilitie= array();
                        foreach($facs as $item){
                            $facilitie[$item] = $item;
                        }
                    @endphp
                    @foreach ($facilities as $item)
                        <div class="facilities_item">
                            <input type="checkbox" id="{{$item['svg']}}"
                                   value="{{$item['id']}}" {{ isset($facilitie[$item['id']]) ? 'checked' : ''}}><label
                                for="{{$item['svg']}}">{{$item['title']}}</label>
                        </div>
                    @endforeach
                </div>
            </div>
            <button class="editAdd_right-button">Обновить объявление</button>
        </div>
    </div>
</section>
