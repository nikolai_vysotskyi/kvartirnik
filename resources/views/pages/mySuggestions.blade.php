<section class="mySuggestions">
    <div class="mySuggestions_name">
        <h1>{{$_SESSION['name']}} {{$_SESSION['surname']}}</h1>
        <a>Мои предложения</a>
    </div>

    @if(count($suggestions)>0)
        <div class="mySuggestions_cards">
            @foreach($suggestions as $el)
                <div class="card">
                    <picture class="card_img">
                        @if($photos[$el->id]['img'])
                            <source media="(min-width: 650px)" srcset="{{ $photos[$el->id]['img'] }}">
                            <img src="{{$photos[$el->id]['img']}}" alt="Flowers">
                        @else
                            <source media="(min-width: 650px)" srcset="/storage/non.png">
                            <img src="/storage/non.png" alt="Flowers">
                        @endif
                        <div class="card_img-hover">
                            <div class="card_img-hoveri">
                                <a class="mySuggestions_delete" product_id="{{$el->id}}">
                                    @include('svg.delete')
                                    Удалить
                                </a>
                                <a href="/editAdd/{{$el->id}}" class="mySuggestions_edit">
                                    @include('svg.edit')
                                    Редактировать
                                </a>
                            </div>
                        </div>
                    </picture>
                    <div class="card_content">
                        <h3 class="card_title">{{$el['name']}}</h3>
                        <p class="card_description">{{$el['description']}}</p>
                    </div>
                    <a class="card_footer" href="/product/{{$el->id}}">
                        <div class="card_price"><p>{{$el->price}} @include('svg.bitcoin')</p></div>
                        <div>
                            <button class="card_button">Подробнее</button>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>

        @if($max_page!=1)
            @include('components.pagination',['page'=>1,'max_page'=>$max_page])
        @endif
    @else
        <section class="landlordf">
            У данного пользователя еще нет объявлений, но Вы можете связаться с ним.
        </section>
    @endif
</section>


