<section class="bucket">
    <div class="bucket_name">
        <h1>Корзина</h1>
    </div>
</section>
@if($last != 0)
<section class="bucket-table">
    @include('components.table', ['element'=>$bucket,'product'=>$product])
</section>
<section class="totalAmount">
    <h2>Итого:</h2>
    <div class="totalAmount-price">
        <p>{{$allprice}} @include('svg.bitcoin')</p>
        <a href="">{{$last}} позиций</a>
    </div>
    <button>Заказать</button>
</section>
@else
    <section class="totalAmount totalAmount-noprice">
        <a>Нет продуктов</a>
    </section>
@endif
