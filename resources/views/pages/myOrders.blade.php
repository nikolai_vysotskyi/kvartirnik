<section class="myOrders">
    <div class="myOrders_name">
        <h1>МИХАИЛ ПАВЛОВ</h1>
        <a>Мои заказы</a>
    </div>
        @foreach($orders as $key =>$order)
            <section class="table">
                <div class="table_wrapper">
                    <table class="table_all" cellspacing="0" cellpadding="0">
                        <thead>
                        <tr class="table_header">
                            <th class="table_header-name">№ 222</th>
                            <th class="table_header-name">{{date("m.d.Y",strtotime($order->updated_at))}}</th>
                            <th class="table_header-name">Николай Семенов</th>
                            <th class="table_header-name"><span>Подробности</span> @include('svg.down-arrow')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products[$key] as $product)
                            <tr class="table_main">
                                <td class="table_main-text"><a href="/product/{{$product['id']}}">{{$product['name']}}</a></td>
                                <td class="table_main-text"></td>
                                <td class="table_main-text"></td>
                                <td class="table_main-text">{{$product['price']}} @include('svg.bitcoin')</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </section>
        @endforeach

</section>
