<section class="bheader">
    <div class="bheader_block">
        <h1>КВАРТИРНИК</h1>
        <p class="title-3">
            Быстрое и надежное решение проблем с недвижимостью
        </p>
        <a class="login_link">
            <button>Поиск недвижимости</button>
        </a>
    </div>
</section>

<section class="property">
    <div class="property_name">
        <h1>НЕДВИЖИМОСТЬ В МАРИУПОЛЕ</h1>
    </div>
    <div class="property_name property_name1">
        <h1>что в квартире</h1>
    </div>
    <div class="property_content">
        <div class="property_blocks property_text">
            <p class="property_paragraph main-paragraph">
                Мы предлагаем лучшие варианты для вашего отдыха в Мариуполе. Мы предлагаем лучшие варианты для вашего
                отдыха в Мариуполе. Мы предлагаем лучшие варианты для вашего отдыха в Мариуполе. Мы предлагаем лучшие
                варианты для вашего отдыха в Мариуполе.
            </p>

            <p class="property_paragraph main-paragraph">
                Мы предлагаем лучшие варианты для вашего отдыха в Мариуполе. Мы предлагаем лучшие в Мариуполе. Мы
                предлагаем лучшие варианты для вашего отдыха в Мариуполе. Мы предлагаем лучшие в Мариуполе.
            </p>
        </div>
        <div class="property_blocks property_blocks-styles">
            <div class="property_block">
                <div class="property_block-styles">
                    <div class="property_block-img">
                        <a class="login_link title-3">Квартиры<br>ЛЮКС</a>
                    </div>
                </div>
                <div class="property_block-styles">
                    <div class="property_block-img">
                        <a class="login_link title-3">Квартиры<br>дизайнерские</a>
                    </div>
                </div>
            </div>

            <div class="property_block ">
                <div class="property_block-styles property_block-styles2">
                    <div class="property_block-img">
                        <a class="login_link title-3">Однокомнатные<br>квартиры</a>
                    </div>
                </div>
            </div>

            <div class="property_block">
                <div class="property_block-styles property_block-styles3">
                    <div class="property_block-img">
                        <a class="login_link title-3">Двухкомнатные<br>квартиры</a>
                    </div>
                </div>
                <div class="property_block-styles property_block-styles3">
                    <div class="property_block-img">
                        <a class="login_link title-3">Дома</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="recommend">

    <div class="recommend_name">
        <h1>Мы рекомендуем</h1>
    </div>

    <div class="recommend-slider">
        <div class="recommend_slidewrapper">
            @foreach($recommend as $item)
                <div class="recommend_slide">
                    <div class="recommend_slide-wrap">
                        <div class="recommend_slide-img"
                             style="background-image: url('{{$photos[$item->id]?$photos[$item->id]['img']:'/storage/non.png'}}');"></div>
                        <div class="recommend_slide-data">
                            <h2 class="recommend_slide-title">{{$item['name']}}</h2>
                            <div class="recommend_slide-img recommend_slide-img2"
                                 style="background-image: url('{{$photos[$item->id]?$photos[$item->id]['img']:'/storage/non.png'}}');"></div>
                            <div class="recommend_slide-val">
                                <p class="recommend_slide-city title-3">
                                    Город: {{$locality[(int)$item['city']]['name']}}</p>
                                <p class="recommend_slide-area title-3">
                                    Район: {{$district[(int)$item['area']]['name']}}</p>
                            </div>
                            <p class="recommend_table title-4">
                                Комнат<span
                                        class="recommend_table-val">{{$countRooms[(int)$item['room']]['number']}}</span>
                            </p>
                            <p class="recommend_table title-4">
                                Общая площадь<span class="recommend_table-val">{{$item->totalArea}}</span>
                            </p>
                            <p class="recommend_table title-4">
                                Жилая площадь<span class="recommend_table-val">{{$item->livingArea}}</span>
                            </p>
                            <p class="recommend_table title-4">
                                Площадь кухни<span class="recommend_table-val">{{$item->kitchenArea}}</span>
                            </p>
                            <p class="recommend_table title-4">
                                Этаж/этажность
                                <span class="recommend_table-val">{{$item->floor}}/{{$floors[$item->floors]->number}}</span>
                            </p>

                            <div class="card_footer">
                                <div class="card_price"><p>{{ number_format($item->price, 0, ',', ' ')}} @include('svg.bitcoin')</p></div>
                                <a href="/product/{{$item->id}}">
                                    <button class="card_button">Подробнее</button>
                                </a>
                            </div>
                            <div class="recommend_slide-lists">
                                <a class="recommend_slide-list recommend_slide-list-left">
                                    @include('svg.slide-left')
                                    <p class="recommend_slide-pred title-4">пред.</p>
                                </a>
                                <a class="recommend_slide-list recommend_slide-list-right">
                                    <p class="recommend_slide-next title-4">след.</p>
                                    @include('svg.slide-right')
                                </a>
                            </div>

                            <div id="recommend_buttons">
                                <div class="recommend_prev"></div>
                                <div class="reccomend_next"></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
</section>


<section class="lastadd">
    <div class="lastadd_name">
        <h1> Последние Добавленные</h1>
    </div>

    <div class="lastadd_name lastadd_name1">
        <h1>все предложения</h1>
    </div>

    <div class="lastadd_cards">
        @foreach($facilities as $el)
            <div class="card">
                <picture class="card_img">
                    <source media="(min-width: 650px)"
                            srcset="{{$photos[$el->id]?$photos[$el->id]['img']:'/storage/non.png'}}">
                    <img src="{{$photos[$el->id]?$photos[$el->id]['img']:'/storage/non.png'}}" alt="Flowers">
                </picture>
                <div class="card_content">
                    <h3 class="card_title">{{$el['name']}}</h3>
                    <p class="card_description">{{$el['description']}}</p>
                </div>
                <div class="card_footer">
                    <div class="card_price"><p>{{number_format($el['price'], 0, ',', ' ')}} @include('svg.bitcoin')</p></div>
                    <a href="/product/{{$el->id}}">
                        <button class="card_button">Подробнее</button>
                    </a>
                </div>
            </div>
        @endforeach
    </div>

</section>
