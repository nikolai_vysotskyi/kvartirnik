<section class="newAdd">
    <div class="newAdd_name">
        <h1>МИХАИЛ ПАВЛОВ</h1>
        <a>Создание объявления</a>
    </div>
    <div class="newAdd_wrapper">
        <div class="newAdd_left">
            <h4>Название</h4>
            <input class="newAdd_left-name" type="text" placeholder="Новое объявление">

            <h4>Описание</h4>
            <textarea class="newAdd_left-message" name="" id="" cols="30" rows="10" placeholder="Сообщение"></textarea>

            <h4>Фотографии</h4>
            <div class="newAdd_left-ph">
                <div class="newAdd_left-photo">
                    <div class="newAdd_left-file">
                        <div class="newAdd_left-img "></div>
                        <div CLASS="newAdd_left-mainphoto">Основное фото</div>
                        <input class="newAdd_left-fileb" nphoto="0" type="file" name="file" multiple>
                    </div>
                    @php
                        $photos = [
                            '1' => 'file',
                            '2' => 'file',
                            '3' => 'file',
                            '4' => 'file',
                            '5' => 'file',
                            '6' => 'file',
                            '7' => 'file',
                        ]
                    @endphp

                    @foreach($photos as $key=> $item)
                        <div class="newAdd_left-file">
                            <div class="newAdd_left-img"></div>
                            <input class="newAdd_left-fileb" nphoto="{{$key}}" type="file" name="file" multiple>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="newAdd_right">
            <div class="newAdd_specifications">
                <div class="newAdd_specification">
                    <h4>Категория</h4>
                    <div class="newAdd_specifications-blselect">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="newAdd_specifications-select newAdd_category">
                            @foreach($category as $item)
                                <option value="{{$item['id']}}"
                                        class="newAdd_specifications-option">{{$item['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="newAdd_specification">
                    <h4>Тип</h4>
                    <div class="newAdd_specifications-blselect">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="newAdd_specifications-select newAdd_type">
                            @foreach($type as $item)
                                <option value="{{$item['id']}}"
                                        class="newAdd_specifications-option">{{$item['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="newAdd_specification">
                    <h4>Цена</h4>
                    <div class="newAdd_specifications-blselect newAdd_specifications-price">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block">
                            <div class="down_arrow">$</div>
                        </div>
                        <input class="newAdd_specifications-select newAdd_price">
                    </div>
                </div>

                <div class="newAdd_specification">
                    <h4>Населенный пункт</h4>
                    <div class="newAdd_specifications-blselect newAdd_specifications-2">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="newAdd_specifications-select newAdd_specifications-2 newAdd_city">
                            @foreach($locality as $item)
                                <option value="{{$item['id']}}"
                                        class="newAdd_specifications-option newAdd_city-select">{{$item['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="newAdd_specification">
                    <h4>Район</h4>
                    <div class="newAdd_specifications-blselect newAdd_specifications-2">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="newAdd_specifications-select newAdd_specifications-2  newAdd_district">
                            @foreach($district as $item)
                                <option value="{{$item['id']}}"
                                        class="newAdd_specifications-option">{{$item['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="newAdd_specification newAdd_specification-3">
                    <h4>Кол-во комнат</h4>
                    <div class="newAdd_specifications-blselect newAdd_specifications-3">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="newAdd_specifications-select newAdd_specifications-3 newAdd_countRooms">
                            @foreach($countRooms as $item)
                                <option value="{{$item['id']}}"
                                        class="newAdd_specifications-option">{{$item['number']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="newAdd_specification newAdd_specification-3">
                    <h4>Этажность</h4>
                    <div class="newAdd_specifications-blselect newAdd_specifications-3">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="newAdd_specifications-select newAdd_specifications-3 newAdd_floors">
                            @foreach($floors as $item)
                                <option value="{{$item['id']}}"
                                        class="newAdd_specifications-option">{{$item['number']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="newAdd_specification newAdd_specification-3">
                    <h4>Этаж</h4>
                    <div class="newAdd_specifications-blselect newAdd_specifications-3">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block">
                            @include('svg.down-arrow')
                        </div>
                        <select class="newAdd_specifications-select newAdd_specifications-3 newAdd_floor">
                                <option value="1"
                                        class="newAdd_specifications-option">1</option>
                        </select>
                    </div>
                </div>

                <div class="newAdd_specification newAdd_specification-3">
                    <h4>Общая площадь</h4>
                    <div class="newAdd_specifications-blselect newAdd_specifications-3">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block ">
                            <div class="down_arrow">м<sup>2</sup></div>
                        </div>

                        <input class="newAdd_specifications-select newAdd_specifications-3 newAdd_totalArea"
                               placeholder="0">
                    </div>
                </div>
                <div class="newAdd_specification newAdd_specification-3">
                    <h4>Жилая площадь</h4>
                    <div class="newAdd_specifications-blselect newAdd_specifications-3">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block ">
                            <div class="down_arrow">м<sup>2</sup></div>
                        </div>

                        <input class="newAdd_specifications-select newAdd_specifications-3 newAdd_livingArea"
                               placeholder="0">
                    </div>
                </div>
                <div class="newAdd_specification newAdd_specification-3">
                    <h4>Площадь кухни</h4>
                    <div class="newAdd_specifications-blselect newAdd_specifications-3">
                        <div class="newAdd_specifications-bg"></div>
                        <div class="newAdd_specifications-block ">
                            <div class="down_arrow">м<sup>2</sup></div>
                        </div>

                        <input class="newAdd_specifications-select newAdd_specifications-3 newAdd_kitchenArea"
                               placeholder="0">
                    </div>
                </div>
            </div>
            <div class="facilities">
                <h4>Удобства</h4>
                <div class="facilities_list">
                    @foreach ($facilities as $item)
                        <div class="facilities_item">
                            <input type="checkbox" id="{{$item['svg']}}" value="{{$item['id']}}"><label
                                for="{{$item['svg']}}">{{$item['title']}}</label>
                        </div>
                    @endforeach

                </div>
            </div>
            <button class="newAdd_right-button">Cоздать объявление</button>
        </div>
    </div>
</section>
