<!doctype html>
<html lang="{{ app()->getLocale() }}}">
<head>
    @include('layouts.head')
</head>
<body>

<div class="pages">
    @php
        $preheader=[
            '1'=>[
                'name'=>'Покупка квартир',
                'link'=>'search',
            ],
            '2'=>[
                'name'=>$product[0]->name,
                'link'=>'product/'.$product[0]->id,
            ]
        ];
    @endphp
    @if(isset($_SESSION['username']))
        @include('components.header')
    @else
        @include('components.headerLogin')
    @endif
    @include('components.preheader',['preheader'=>$preheader])
    @include('components.login')
    @include('components.registration')
    @include('components.recovery')
    @include('components.delete-ad')
    @include('components.popup')
    @include('pages.product')
    @if(isset($_SESSION['username'])):
    @include('components.footer')
    @else
        @include('components.footerLogin')
    @endif
</div>


<script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
