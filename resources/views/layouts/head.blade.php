<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Квартирник</title>
<link rel="stylesheet" href="/css/app.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}">

<link rel="apple-touch-icon" sizes="180x180" href="/images/fav/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/fav/favicon-16x16.png">
<link rel="manifest" href="/images/fav/site.webmanifest">
<link rel="mask-icon" href="/images/fav/safari-pinned-tab.svg" color="#d55b5b">
<link rel="shortcut icon" href="/images/fav/favicon.ico">
<meta name="msapplication-TileColor" content="#d55b5b">
<meta name="msapplication-TileImage" content="/images/fav/mstile-144x144.png">
<meta name="msapplication-config" content="/images/fav/browserconfig.xml">
<meta name="theme-color" content="#ffffff">