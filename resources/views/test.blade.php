<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>

@include('components.header')
<h1>heading h1</h1>
<h2>heading h2</h2>
<h3>heading h3</h3>
<h4>heading h4</h4>

<p class="title-1"> Мариуполь</p>
<p class="title-2">title 2</p>
<p class="title-3">title 3</p>
<p class="title-4">title 4</p>
<p class="title-5">title 5</p>

@include('components.card')
<br>
<br>
<br>
<br>
@include('components.facilities')
<br>
<br>
<br>
<br>
<br>
@include('components.popup')
<p class="main-paragraph">Мы предлагаем лучшие варианты для вашего отдыха в Мариуполе.
    Мы предлагаем лучшие варианты для вашего отдыха в Мариуполе.
    Мы предлагаем лучшие варианты для вашего отдыха в Мариуполе.
    Мы предлагаем лучшие варианты для вашего отдыха в Мариуполе.</p>

<a class="link" href="#">вход</a>

<div class="checkbox">
    <input type="checkbox" id="scales" checked/>
    <label for="scales">Диван</label>
</div>

<ul class="list-marked">
    <li>Пункт первый</li>
    <li>Пункт второй</li>
    <li>Пункт третий</li>
    <li>Пункт четвертый</li>
</ul>

<ol class="list-numerated">
    <li>Пункт первый</li>
    <li>Пункт второй</li>
    <li>Пункт третий</li>
    <li>Пункт четвертый</li>
    <li>Пункт четвертый</li>
    <li>Пункт четвертый</li>
    <li>Пункт четвертый</li>
    <li>Пункт четвертый</li>
    <li>Пункт четвертый</li>
    <li>Пункт четвертый</li>
    <li>Пункт четвертый</li>
    <li>Пункт четвертый</li>
    <li>Пункт четвертый</li>
</ol>

<p>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam, similique ullam! Alias amet dolorum earum ipsum
    laborum, magni maxime modi neque nobis odio possimus quidem, quos repellendus tempore voluptatem! At debitis
    distinctio doloremque et exercitationem expedita facere fuga fugiat fugit inventore molestias non officiis,
    quibusdam repellendus, repudiandae sequi similique voluptate voluptates! Accusantium architecto consectetur,
    consequuntur cumque est harum incidunt ipsam labore necessitatibus nihil nobis obcaecati officia omnis perspiciatis
    quae quo quod repellendus similique sit tenetur voluptatem voluptates voluptatum. Assumenda deserunt ipsum iste
    maiores natus possimus rem! Amet consequuntur, cupiditate delectus dicta, error esse eveniet ex exercitationem
    facere incidunt nam pariatur reiciendis sequi temporibus vero. Aliquid aperiam architecto atque commodi cumque
    cupiditate delectus deleniti est facere hic inventore ipsam iure laborum, minima odio odit officiis placeat qui
    reiciendis sequi, sint sit suscipit temporibus veniam vero voluptas voluptate voluptatibus. A ab accusantium ad
    alias aliquid dolorum eligendi error, illum impedit omnis placeat porro quisquam rem tenetur velit voluptas
    voluptate? Architecto aspernatur atque cupiditate dolore doloribus, enim et excepturi fugit id libero molestiae
    nostrum pariatur placeat praesentium quaerat qui quis, sequi sunt totam velit. Aliquid at consectetur doloribus,
    earum inventore ipsam iste magni minima, nemo nisi nobis quibusdam quisquam repudiandae unde vitae. Impedit ipsum
    molestiae nemo nihil placeat quaerat quas, quasi quia quos ut. Ad animi aperiam debitis, id natus quam quibusdam sed
    sequi? Accusamus animi, beatae consequuntur debitis deleniti dolore eos est exercitationem fuga ipsa maiores
    perferendis provident quis, recusandae rem sequi tempore temporibus unde velit voluptatum! Aspernatur consequuntur
    error facere fugiat illum iure laudantium modi obcaecati odit quae quam rerum, sapiente sint tempora vel. Cumque
    doloremque doloribus id incidunt maiores minima
    minus mollitia, necessitatibus praesentium, quas suscipit, vero vitae voluptas. Aliquam atque beatae cupiditate
    fugit laudantium nihil
    placeat praesentium provident rem sint. Ad aliquid debitis ea facilis hic nemo possimus tempore.
</p>

    <button>Welcome</button>

    <p class="title-5">title 5</p>

<input type="text" placeholder="введте логин">

<script src="/js/app.js"></script>
<input type="text" placeholder="введте логин">


@include('components.pagination')

@include('components.table')

@include('components.footer')

</body>
</html>
