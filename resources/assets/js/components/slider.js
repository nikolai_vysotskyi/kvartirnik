var slideNow = 1;
var slideCount = $('.recommend_slidewrapper').children().length;
console.log(1);

function prevSlide() {
    if (slideNow >= slideCount || slideNow <= 0) {
        $('.recommend-slider').css('transform', 'translate(0, 0)');
        $('.recommend_slidewrapper').css('transform', 'translate(0, 0)');
        slideNow = 1;
    } else {
        translateWidth = -$('.recommend-slider').width() * (slideNow);
        $('.recommend_slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow++;
    }
}

function nextSlide() {
    if (slideNow == 1 || slideNow <= 0 || slideNow > slideCount) {
        translateWidth = -$('.recommend-slider').width() * (slideCount - 1);
        $('.recommend_slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow = slideCount;
    } else {
        translateWidth = -$('.recommend-slider').width() * (slideNow - 2);
        $('.recommend_slidewrapper').css({
            'transform': 'translate(' + translateWidth + 'px, 0)',
            '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
            '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
        });
        slideNow--;
    }
}


$('.recommend_slide-list-right').click(function () {
    prevSlide();
});

$('.recommend_slide-list-left').click(function () {
    nextSlide();
});


var slideNow1 = 1;
var navBtnId1 = 0;
var translateWidth1 = 0;

$(document).ready(function (e) {
    $('.slide-nav-btn').click(function () {
        $('.slide-nav-btn').eq(navBtnId1).css({'background': 'none',});

        navBtnId1 = $(this).index();
        $('.slide-nav-btn').eq(navBtnId1).css({'background': 'rgba(255,255,255)',});
        if (navBtnId1 + 1 != slideNow1) {
            translateWidth1 = -$('.aboutflat_blockr-img').width() * (navBtnId1);

            $('.aboutflat_slidewrapper').css({
                'transform': 'translate(' + translateWidth1 + 'px, 0)',
                '-webkit-transform': 'translate(' + translateWidth1 + 'px, 0)',
                '-ms-transform': 'translate(' + translateWidth1 + 'px, 0)',
            });
            slideNow1 = navBtnId1 + 1;
        }
    });
});
