$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function () {
    $('.reg_button').on('click', function () {
        var login = $('.reg_login').val();
        var password = $('.reg_pass1').val();
        var password1 = $('.reg_pass2').val();
        if (password == password1) {
            $.ajax({
                url: "/api/user/create",
                type: "GET",
                data: {
                    username: login,
                    password: password,
                },
                success: function () {
                    var url = "/personalData";
                    $(location).attr('href', url);
                },
                error: function (data) {
                    $('.validate').fadeOut(400).remove();
                    var mes = "";

                    if (data['responseJSON']['errors']['username']) {
                        if (data['responseJSON']['errors']['username'][0] == "The username field is required.") {
                            mes = "Логин должен содержать не менее 1 символа";
                        } else if (data['responseJSON']['errors']['username'][0] == "The username has already been taken.") {
                            mes = "Этот логин уже занят";
                        }
                    } else if (data['responseJSON']['errors']['password']) {
                        if (data['responseJSON']['errors']['password'][0] == "The password field is required." || data['responseJSON']['errors']['password'][0] == "The password must be at least 6 characters.") {
                            mes = "Пароль должен содержать не менее 6 символов";
                        }
                    }
                    var val = $('<div class="validate">' + mes + '</div>').hide().prependTo('.registration_block').fadeIn(400);
                    setTimeout(function () {
                        val.fadeOut(400).remove();
                    }, 5000);
                }
            });
        } else {
            $('.validate').fadeOut(400).remove();
            var val = $('<div class="validate">Пароли не совпадают</div>').hide().prependTo('.registration_block').fadeIn(400);
            setTimeout(function () {
                val.fadeOut(400).remove();
            }, 5000);
        }
    });

    $('.log_button').click(function () {
        var login = $('.log_login').val();
        var password = $('.log_pass').val();

        $.ajax({
            url: "/api/user/auth",
            type: "GET",
            data: {
                username: login,
                password: password,
            },
            success: function () {
                location.reload();
            }
        });
    });


    $('.popup_footer a').click(function () {
        $.ajax({
            url: "/api/user/logout",
            type: "GET",
            data: {},
            success: function () {
                location.reload();
            }
        });
    });

    $('.personalData_button').click(function () {
        var name = $('.personalData_input-name').val();
        var surname = $('.personalData_input-surname').val();
        var phone = $('.personalData_input-phone').val();
        var email = $('.personalData_input-email').val();

        $.ajax({
            url: "/api/user/editUser",
            type: "GET",
            data: {
                name: name,
                surname: surname,
                phone: phone,
                email: email
            },
            success: function () {
                window.location.replace('/');
            }
        });
    });
    $(".newAdd_city").change(function () {
        var city = $(this).val();
        console.log(2);
        $.ajax({
            url: "/api/products/checkLocality",
            type: "GET",
            data: {
                city: city
            },
            success: function (data) {
                console.log(data);
                var district = "";
                data.forEach(function (item) {
                    district += '<option value="' + item['id'] + '" class="newAdd_specifications-option">' + item['name'] + '</option>';
                });
                $(".newAdd_district").html(district);
            }
        });
    });

    $(".newAdd_floors").change(function () {
        var floor = $(this).val() - 1;
        var floors = parseInt($(".newAdd_floors .newAdd_specifications-option:eq(" + floor + ")").text());
        $('.newAdd_floor').empty();
        for (var i = 1; i <= floors; i++) {
            $('.newAdd_floor').append("<option value='" + i + "' class='newAdd_specifications-option'>" + i + "</option>");
        }
    });

    $(".editAdd_city").change(function () {
        var city = $(this).val();
        console.log(2);
        $.ajax({
            url: "/api/products/checkLocality",
            type: "GET",
            data: {
                city: city
            },
            success: function (data) {
                console.log(data);
                var district = "";
                data.forEach(function (item) {
                    district += '<option value="' + item['id'] + '" class="editAdd_specifications-option">' + item['name'] + '</option>';
                });
                $(".editAdd_district").html(district);
            }
        });
    });

    $(".editAdd_floors").change(function () {
        var floor = $(this).val() - 1;
        var floors = parseInt($(".editAdd_floors .editAdd_specifications-option:eq(" + floor + ")").text());
        $('.editAdd_floor').empty();
        for (var i = 1; i <= floors; i++) {
            $('.editAdd_floor').append("<option value='" + i + "' class='editAdd_specifications-option'>" + i + "</option>");
        }
    });

    var photos = [];

    $('.newAdd_left-fileb, .editAdd_left-fileb').on('change', function () {
        var fileName = $(this).val();
        var nphoto = parseInt($(this).attr('nphoto'));
        if (fileName) {
            var files, cl;
            cl = $(this);
            files = this.files;
            var data = new FormData();
            $.each(files, function (key, value) {
                data.append(key, value);
            });

            var producId = $('.editAdd, .newAdd').attr('productId');

            data.append('id', producId);
            data.append('photoNumber', nphoto);
            $.ajax({
                url: "/api/products/addPhoto",
                type: "post",
                contentType: false,
                processData: false,
                data: data,
                success: function (data) {
                    if (!cl.siblings('.newAdd_left-img, .editAdd_left-img').hasClass('Add_left-checkphoto'))
                        cl.siblings('.newAdd_left-img, .editAdd_left-img').toggleClass("Add_left-checkphoto");
                    cl.siblings('.newAdd_left-img, .editAdd_left-img').css({'background-image': 'url("' + data + '")'});

                    photos[nphoto] = data;
                    console.log(photos);
                },
                error: function (data) {
                    console.log(data['responseJSON']['errors'][0]);
                    $('.validate').fadeOut(400).remove();
                    mes = "";
                    if (data['responseJSON']['errors']['0']) {
                        if (data['responseJSON']['errors'][0][0] == "The 0 may not be greater than 2048 kilobytes.") {
                            mes = "Размер фотографии более 2-х МБ!";
                        }
                    }
                    var val = $('<div class="validate">' + mes + '</div>').hide().insertAfter('.newAdd_left-ph').fadeIn(400).css({'margin': '40px 0 0 0'});
                    setTimeout(function () {
                        val.fadeOut(400).remove();
                    }, 5000);
                }
            });
        }
    });

    $('.newAdd_right-button').click(function () {
        var name = $('.newAdd_left-name').val();
        var description = $('.newAdd_left-message').val();
        var category = parseInt($('.newAdd_category option:selected').val()) - 1;
        var type = parseInt($('.newAdd_type option:selected').val()) - 1;
        var price = $('.newAdd_price').val();
        var locality = parseInt($('.newAdd_city option:selected').val()) - 1;
        var district = parseInt($('.newAdd_district option:selected').val()) - 1;
        var countRooms = parseInt($('.newAdd_countRooms option:selected').val());
        var floors = parseInt($('.newAdd_floors option:selected').val()) - 1;
        var floor = parseInt($('.newAdd_floor option:selected').val()) - 1;
        var totalArea = $('.newAdd_totalArea').val() ? $('.newAdd_totalArea').val() : 0;
        var livingArea = $('.newAdd_livingArea').val() ? $('.newAdd_livingArea').val() : 0;
        var kitchenArea = $('.newAdd_kitchenArea').val() ? $('.newAdd_kitchenArea').val() : 0;
        var facilities = "";
        $('.facilities_item input:checkbox:checked').each(function () {
            facilities += parseInt($(this).val()) - 1 + ' ';
        });
        var token = $('.newAdd input#_token').val();
        console.log(photos);
        $.ajax({
            url: "/api/products/add",
            type: "post",
            data: {
                _token: token,
                name: name,
                description: description,
                category: category,
                type: type,
                price: price,
                locality: locality,
                district: district,
                countRooms: countRooms,
                floors: floors,
                floor: floor,
                totalArea: totalArea,
                livingArea: livingArea,
                kitchenArea: kitchenArea,
                photos: photos,
                facilities: facilities,
            },
            success: function (id) {
                if (id == "censure") {
                    $('.validate').fadeOut(400).remove();
                    mes = "Вы использовали нецензурную брань!";
                    var val = $('<div class="validate">' + mes + '</div>').hide().insertAfter('.facilities').fadeIn(400).css({'margin': '40px 0 0 0'});
                    setTimeout(function () {
                        val.fadeOut(400).remove();
                    }, 5000);
                } else {
                   window.location.replace('/product/' + id);
                }
            },
            error: function (data) {
                $('.validate').fadeOut(400).remove();
                mes = "";
                if (data['responseJSON']['errors']['name']) {
                    if (data['responseJSON']['errors']['name'][0] == "The name field is required.") {
                        mes = "Назваие должно содержать не менее 5 символов";
                    }
                } else if (data['responseJSON']['errors']['description']) {
                    if (data['responseJSON']['errors']['description'][0] == "The description field is required.") {
                        mes = "Сообщение должно содержать не менее 5 символов";
                    }
                } else if (data['responseJSON']['errors']['price']) {
                    if (data['responseJSON']['errors']['price'][0] == "The price field is required.") {
                        mes = "Вы не указали цену";
                    }
                }


                var val = $('<div class="validate">' + mes + '</div>').hide().insertAfter('.facilities').fadeIn(400);
                $('.newAdd_right-button').css({'margin': '17px 0 0 0'});
                setTimeout(function () {
                    val.fadeOut(400).remove();
                    $('.newAdd_right-button').css({'margin': '83px 0 0 0'});
                }, 5000);
            }
        });
    });

    $('.editAdd_right-button').click(function () {
        var name = $('.editAdd_left-name').val();
        var description = $('.editAdd_left-message').val();
        var category = parseInt($('.editAdd_category option:selected').val()) - 1;
        var type = parseInt($('.editAdd_type option:selected').val()) - 1;
        var price = $('.editAdd_price').val();
        var locality = parseInt($('.editAdd_city option:selected').val()) - 1;
        var district = parseInt($('.editAdd_district option:selected').val()) - 1;
        var countRooms = parseInt($('.editAdd_countRooms option:selected').val());
        var floors = parseInt($('.editAdd_floors option:selected').val()) - 1;
        var floor = parseInt($('.editAdd_floor option:selected').val()) - 1;
        var totalArea = $('.editAdd_totalArea').val() ? $('.editAdd_totalArea').val() : 0;
        var livingArea = $('.editAdd_livingArea').val() ? $('.editAdd_livingArea').val() : 0;
        var kitchenArea = $('.editAdd_kitchenArea').val() ? $('.editAdd_kitchenArea').val() : 0;
        var producId = $('.editAdd').attr('productId');
        var facilities = "";
        $('.facilities_item input:checkbox:checked').each(function () {
            facilities += parseInt($(this).val()) - 1 + ' ';
        });
        var token = $('.editAdd input#_token').val();
        $.ajax({
            url: "/api/products/edit",
            type: "PUT",
            data: {
                _token: token,
                name: name,
                description: description,
                category: category,
                type: type,
                price: price,
                locality: locality,
                district: district,
                countRooms: countRooms,
                floors: floors,
                floor: floor,
                totalArea: totalArea,
                livingArea: livingArea,
                kitchenArea: kitchenArea,
                facilities: facilities,
                photos: photos,
                id: producId,
            },
            success: function (id) {
                window.location.replace('/product/' + id);
            },
            error: function (data) {
                $('.validate').fadeOut(400).remove();
                mes = "";
                if (data['responseJSON']['errors']['name']) {
                    if (data['responseJSON']['errors']['name'][0] == "The name field is required.") {
                        mes = "Назваие должно содержать не менее 5 символов";
                    }
                } else if (data['responseJSON']['errors']['description']) {
                    if (data['responseJSON']['errors']['description'][0] == "The description field is required.") {
                        mes = "Сообщение должно содержать не менее 5 символов";
                    }
                } else if (data['responseJSON']['errors']['price']) {
                    if (data['responseJSON']['errors']['price'][0] == "The price field is required.") {
                        mes = "Вы не указали цену";
                    }
                }


                var val = $('<div class="validate">' + mes + '</div>').hide().insertAfter('.facilities').fadeIn(400).css({'margin': '40px 0 0 0'});
                setTimeout(function () {
                    val.fadeOut(400).remove();
                }, 5000);
            }
        });
    });

    $('.mySuggestions .pagination_el-active').on('click', function () {
        var page;
        page = $(this).attr("value");
        $.ajax({
            url: "/api/products/mySugPag",
            type: "GET",
            data: {
                page: page,
            },
            success: function (data) {
                if (data) {
                    $('.mySuggestions_cards').empty();
                    data['suggestions'].forEach(function (sug) {
                        var path = data["photos"][sug["id"]] ? data["photos"][sug["id"]]["img"] : '/storage/non.png';
                        $('.mySuggestions_cards').append(
                            '<div class="card">' +
                            '   <picture class="card_img">' +
                            '       <source media="(min-width: 650px)" srcset="' + path + '">' +
                            '       <img src="' + path + '" alt="Flowers">' +
                            '       <div class="card_img-hover">' +
                            '           <div class="card_img-hoveri"> ' +
                            '               <a class="mySuggestions_delete" product_id="' + sug['id'] + '">' +
                            '               <img class="mySuggestions_delete-icon" src="/svg/delete.svg">' +
                            '               Удалить </a>' +
                            '               <a href="/editAdd/' + sug['id'] + '" class="mySuggestions_edit"> ' +
                            '               <img class="mySuggestions_edit-icon" src="/svg/edit.svg">' +
                            '               Редактировать </a>' +
                            '           </div>' +
                            '       </div>' +
                            '   </picture>' +
                            '   <div class="card_content">' +
                            '       <h3 class="card_title">' + sug['name'] + '</h3>' +
                            '       <p class="card_description">' + sug['description'] + '</p>' +
                            '   </div>' +
                            '   <div class="card_footer">' +
                            '       <div class="card_price"><p>' + sug['price']
                            + '<img class="bitcoin" style="width: 14px;margin: 5px;" src="/svg/bitcoin.svg">' +
                            '       </p></div>' +
                            '       <a href="/product/' + sug['id'] + '"> <button class="card_button">Подробнее</button> </a>' +
                            '   </div>' +
                            '</div>');
                    });

                    $('.pagination_page-this').attr("value", data['page']);
                    $('.pagination_page-left').attr("value", parseInt(data['page']) - 1);
                    $('.pagination_page-right').attr("value", parseInt(data['page']) + 1);
                    $('.pagination_page-last').attr("value", data['max_page']);
                    $('.pagination_page-this').html(data['page']);
                    $('.pagination_page-last').html(data['max_page']);

                    $('.mySuggestions_delete').click(function () {
                        $('.deletead').toggleClass('pblock');

                        var msD = $(this).attr('product_id');

                        $('.deletead_yes').click(function () {
                            $.ajax({
                                url: "/api/products/delete",
                                type: "GET",
                                data: {
                                    id: msD,
                                },
                                success: function (id) {
                                    location.reload();
                                }
                            });
                        });
                    });
                }
            }
        });
    });
    $('.comments_new-button').click(function () {
        var message = $('.comments_new-feedback').val();
        var name = $('.comments_new-feedname').val();
        var email = $('.comments_new-feedmail').val();
        var productId = $('.mainfeatures').attr('productid');
        var token = $('.newAdd input#_token').val();
        $.ajax({
            url: "/api/products/addComment",
            type: "PUT",
            data: {
                _token: token,
                name: name,
                email: email,
                message: message,
                productId: productId,
            },
            success: function () {
                window.location.reload();
            }
        });
    });

    // Bucket

    $('.mainfeatures_price .card_button').click(function () {
        var id = $('.mainfeatures').attr('productId');
        var token = $('.mainfeatures input#_token').val();
        $.ajax({
            url: "/api/bucket/product/" + id,
            type: "GET",
            data: {
                _token: token,
            },
            success: function (id) {
                window.location.replace('/bucket/');
            }
        });
    });

    $('.bucket-table .table_main-delete, .bucket-table .table_main-mdelete').click(function () {
        var id = $(this).attr('productId');
        var token = $('.mainfeatures input#_token').val();
        $.ajax({
            url: "/api/bucket/delete/" + id,
            type: "DELETE",
            data: {
                _token: token,
                id: id,
            },
            success: function (id) {
              //  location.reload();
            }
        });
    });
    $('.totalAmount button').click(function () {
        $.ajax({
            url: "/api/bucket/order",
            type: "put",
            success: function () {
              //  location.reload();
            }
        });
    });
    $('.landlordn .pagination_el-active').on('click', function () {
        var userId = $('.landlordn').attr('useriD');
        var page = $(this).attr("value");
        $.ajax({
            url: "/api/user/Pag",
            type: "GET",
            data: {id: userId, page: page},
            success: function (data) {
                if (data) {
                    $('.landlordn_cards').empty();
                    data['userprod'].forEach(function (sug) {
                        var path = data["photos"][sug["id"]] ? data["photos"][sug["id"]]["img"] : '/storage/non.png';
                        $('.landlordn_cards').append(
                            '<div class="card">' +
                            '   <picture class="card_img">' +
                            '       <source media="(min-width: 650px)" srcset="' + path + '">' +
                            '       <img src="' + path + '" alt="Flowers">' +
                            '   </picture>' +
                            '   <div class="card_content">' +
                            '       <h3 class="card_title">' + sug['name'] + '</h3>' +
                            '       <p class="card_description">' + sug['description'] + '</p>' +
                            '   </div>' +
                            '   <div class="card_footer">' +
                            '       <div class="card_price"><p>' + sug['price']
                            + '<img class="bitcoin" style="width: 14px;margin: 5px;" src="/svg/bitcoin.svg">' +
                            '       </p></div>' +
                            '       <a href="/product/' + sug['id'] + '"> <button class="card_button">Подробнее</button> </a>' +
                            '   </div>' +
                            '</div>');
                    });

                    $('.pagination_page-this').attr("value", data['page']);
                    $('.pagination_page-left').attr("value", parseInt(data['page']) - 1);
                    $('.pagination_page-right').attr("value", parseInt(data['page']) + 1);
                    $('.pagination_page-last').attr("value", data['max_page']);
                    $('.pagination_page-this').html(data['page']);
                    $('.pagination_page-last').html(data['max_page']);
                }
            }
        });
    });
    $('.search_button').click(function () {
        var keyword = $('.search_keywords').val();
        var locality = $('.search_locality').val();
        var category = $('.search_category option:selected').val();
        var countRooms1 = $('.search_countRooms1').val();
        var countRooms2 = $('.search_countRooms2').val();
        var floor1 = $('.search_floor1').val();
        var floor2 = $('.search_floor2').val();
        var price1 = $('.search_price1').val();
        var price2 = $('.search_price2').val();

        $.ajax({
            url: "/api/search/button",
            type: "get",
            data: {
                keyword: keyword,
                locality: locality,
                category: category,
                countRooms1: countRooms1,
                countRooms2: countRooms2,
                floor1: floor1,
                floor2: floor2,
                price1: price1,
                price2: price2,
            },
            success: function (data) {
                if (data) {
                    $('.search_cards').empty();
                    data['products']['data'].forEach(function (sug) {
                        var path = data["photos"][sug["id"]] ? data["photos"][sug["id"]]["img"] : '/storage/non.png';
                        $('.search_cards').append(
                            '<div class="card">' +
                            '   <picture class="card_img">' +
                            '       <source media="(min-width: 650px)" srcset="' + path + '">' +
                            '       <img src="' + path + '" alt="Flowers">' +
                            '   </picture>' +
                            '   <div class="card_content">' +
                            '       <h3 class="card_title">' + sug['name'] + '</h3>' +
                            '       <p class="card_description">' + sug['description'] + '</p>' +
                            '   </div>' +
                            '   <div class="card_footer">' +
                            '       <div class="card_price"><p>' + sug['price']
                            + '<img class="bitcoin" style="width: 14px;margin: 5px;" src="/svg/bitcoin.svg">' +
                            '       </p></div>' +
                            '       <a href="/product/' + sug['id'] + '"> <button class="card_button">Подробнее</button> </a>' +
                            '   </div>' +
                            '</div>');
                    });

                    $('.pagination_page-this').attr("value", data['products']['current_page']);
                    $('.pagination_page-left').attr("value", parseInt(data['products']['current_page']) - 1);
                    $('.pagination_page-right').attr("value", parseInt(data['products']['current_page']) + 1);
                    $('.pagination_page-last').attr("value", data['products']['last_page']);
                    $('.pagination_page-this').html(data['products']['current_page']);
                    $('.pagination_page-last').html(data['products']['last_page']);
                }
            }
        });
    });
    $('.search .pagination_el-active').click(function () {
        var page;
        page = $(this).attr("value");
        var keyword = $('.search_keywords').val();
        var locality = $('.search_locality').val();
        var category = $('.search_category option:selected').val();
        var countRooms1 = $('.search_countRooms1').val();
        var countRooms2 = $('.search_countRooms2').val();
        var floor1 = $('.search_floor1').val();
        var floor2 = $('.search_floor2').val();
        var price1 = $('.search_price1').val();
        var price2 = $('.search_price2').val();

        $.ajax({
            url: "/api/search/pag",
            type: "get",
            data: {
                keyword: keyword,
                locality: locality,
                category: category,
                countRooms1: countRooms1,
                countRooms2: countRooms2,
                floor1: floor1,
                floor2: floor2,
                price1: price1,
                price2: price2,
                page: page,
            },
            success: function (data) {
                if (data['products']['data'].length>0) {
                    $('.search_cards').empty();
                    console.log(data['products']);
                    data['products']['data'].forEach(function (sug) {
                        var path = data["photos"][sug["id"]] ? data["photos"][sug["id"]]["img"] : '/storage/non.png';
                        $('.search_cards').append(
                            '<div class="card">' +
                            '   <picture class="card_img">' +
                            '       <source media="(min-width: 650px)" srcset="' + path + '">' +
                            '       <img src="' + path + '" alt="Flowers">' +
                            '   </picture>' +
                            '   <div class="card_content">' +
                            '       <h3 class="card_title">' + sug['name'] + '</h3>' +
                            '       <p class="card_description">' + sug['description'] + '</p>' +
                            '   </div>' +
                            '   <div class="card_footer">' +
                            '       <div class="card_price"><p>' + sug['price']
                            + '<img class="bitcoin" style="width: 14px;margin: 5px;" src="/svg/bitcoin.svg">' +
                            '       </p></div>' +
                            '       <a href="/product/' + sug['id'] + '"> <button class="card_button">Подробнее</button> </a>' +
                            '   </div>' +
                            '</div>');
                    });

                    $('.pagination_page-this').attr("value", data['products']['current_page']);
                    $('.pagination_page-left').attr("value", parseInt(data['products']['current_page']) - 1);
                    $('.pagination_page-right').attr("value", parseInt(data['products']['current_page']) + 1);
                    $('.pagination_page-last').attr("value", data['products']['last_page']);
                    $('.pagination_page-this').html(data['products']['current_page']);
                    $('.pagination_page-last').html(data['products']['last_page']);
                }
            }
        });
    });
    $('.category_link, .search_submit').on('click', function () {
        var el = $(this).attr('category');
        var name = 'category';
        if (!el) {
            el = $('.search_input').val();
            name = 'keyword';
        }
        $.ajax({
            url: "/api/search/session",
            type: "get",
            data: {
                el: el,
                name: name,
            },
            success: function (id) {
                window.location.replace('/search');
            }
        });
    });
});
