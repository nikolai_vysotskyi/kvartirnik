console.log('works');
const menuBtn = document.querySelector('.menu-button');
const header = document.querySelector('.header');

menuBtn.addEventListener('click', () => {
    menuBtn.classList.toggle('mod_close');
    header.classList.toggle('mod_opened');
});

window.onscroll = function () {
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    if (scrolled != 0)
        $('.nav-mobile').addClass("menu_scrolled");
    else {
        $('.nav-mobile').removeClass("menu_scrolled");
    }
};


function windowSize() {
    if ($(window).width() <= '600') {
        $('.popup').toggleClass('header_user-m');
    }
}

$('.header-user_name').click(function () {
    $(window).resize(windowSize());
});

$('.menu-p-button').click(function () {
    $(window).resize(windowSize());
});


$('.login_link').click(function () {
    $('.login').toggleClass('pblock');
});
$('.user_reg').click(function () {
    $('.registration').toggleClass('pblock');
});
$('.mySuggestions_delete').click(function () {
    $('.deletead').toggleClass('pblock');

    var msD = $(this).attr('product_id');

    $('.deletead_yes').click(function () {
        $.ajax({
            url: "/api/products/delete",
            type: "GET",
            data: {
                id: msD,
            },
            success: function (id) {
                location.reload();
            }
        });
    });
});

$('.login_register, .registration_login').click(function () {
    $('.login').toggleClass('pblock');
    $('.registration').toggleClass('pblock');
});
$('.login_password-recovery , .recovery_login').click(function () {
    $('.login').toggleClass('pblock');
    $('.recovery').toggleClass('pblock');
});
$('.recovery_register').click(function () {
    $('.recovery').toggleClass('pblock');
    $('.registration').toggleClass('pblock');
});


$('.close_icon,.deletead_no').click(function () {
    $('.pblock').toggleClass('pblock');
});


