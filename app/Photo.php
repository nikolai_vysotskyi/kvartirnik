<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'img',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
