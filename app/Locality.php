<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
    public $timestamps = false;
    protected $table = 'locality';
    protected $hidden = [
        'id',
        'name',
    ];
}
