<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Floors extends Model
{
    public $timestamps = false;
    protected $table = 'floors';
    protected $hidden = [
        'id',
    ];
}
