<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CountRoom extends Model
{
    public $timestamps = false;
    protected $table = 'count_rooms';
    protected $hidden = [
        'id',
    ];
}
