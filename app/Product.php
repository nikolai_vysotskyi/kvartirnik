<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'description',
        'category',
        'type',
        'price',
        'locality',
        'district',
        'countRooms',
        'floors',
        'floor',
        'totalArea',
        'livingArea',
        'kitchenArea',
        ];

    public function photos()
    {
        return $this->hasMany('App\Photo', 'productId','id');
    }
    public function comments()
    {
        return $this->hasMany('App\Comment', 'productId','id');
    }
    public function user()
    {
        return $this->hasOne('App\User', 'id','id');
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}

