<?php

namespace App\Http\Controllers;

use App\Bucket;
use App\Category;
use App\Comment;
use App\CountRoom;
use App\District;
use App\Facilitie;
use App\Locality;
use App\Product;
use App\Floors;
use App\Type;
use App\Photo;
use App\User;
use App\Order;
use App\Purchase;


//use Couchbase\Bucket;
use React\EventLoop\Factory;
use \unreal4u\TelegramAPI\HttpClientRequestHandler;
use \unreal4u\TelegramAPI\Telegram\Methods\SendPhoto;
use \unreal4u\TelegramAPI\Telegram\Types\Custom\InputFile;
use \unreal4u\TelegramAPI\TgLog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use function MongoDB\BSON\toJSON;


class ProductsController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function Product(Request $request)
    {
        if(!Product::find($request->id)) return redirect('/');
        $product = Product::where([
            'id' => $request->id,
        ])->get();

        $floors = Floors::get();
        $locality = Locality::get();
        $district = District::get();
        $countRooms = CountRoom::get();
        $facilities = Facilitie::get();

        $photos = Product::find($request->id)->photos()->get();
        $comments = Product::find($request->id)->comments()->get();

        foreach ($comments as $key => $comment) {
            $comments[$key]['userName'] = Comment::find($comment->userId)->user()->select('name', 'surname')->first();
        }

        $seller = Product::find($product[0]->userId)->user()->get();

        return view('product', ['product' => $product, 'facilities' => $facilities, 'floors' => $floors, 'locality' => $locality, 'district' => $district, 'countRooms' => $countRooms, 'photos' => $photos, 'seller' => $seller, 'comments' => $comments]);
    }


    public function addProducts(Request $request)
    {
        $type = Type::get();
        $category = Category::get();
        $floors = Floors::get();
        $locality = Locality::get();
        $district = District::where(['localityId' => 1])->get();
        $countRooms = CountRoom::get();
        $facilities = Facilitie::get();

        return view('newAdd', ['facilities' => $facilities, 'type' => $type, 'category' => $category, 'floors' => $floors, 'locality' => $locality, 'district' => $district, 'countRooms' => $countRooms,]);
    }

    public function editProducts(Request $request)
    {
        $add = Product::where([
            'userId' => $_SESSION['userId'],
            'id' => $request->id,
            'customerId' => 0,
        ])->first();

        if ($add) {
            $type = Type::get();
            $category = Category::get();
            $floors = Floors::get();
            $locality = Locality::get();
            $district = District::where(['localityId' => 1])->get();
            $countRooms = CountRoom::get();
            $facilities = Facilitie::get();

            $photos = Product::find($request->id)->photos()->get();

            return view('editAdd', ['productId' => $request->id, 'add' => $add, 'photos' => $photos, 'facilities' => $facilities, 'type' => $type, 'category' => $category, 'floors' => $floors, 'locality' => $locality, 'district' => $district, 'countRooms' => $countRooms,]);
        } else {
            return redirect('/');
        }
    }

    public function addProduct(Request $request)
    {
        if (isset($_SESSION['username'])) {
            $validatedData = $request->validate([
                'name' => 'required|max:50|min:5',
                'description' => 'required|min:5',
                'category' => 'required|min:1',
                'type' => 'required|min:1',
                'price' => 'required|min:1',
                'locality' => 'required|min:1',
                'district' => 'required|min:1',
                'countRooms' => 'required|min:1',
                'floors' => 'required|min:1',
                'floor' => 'required|min:1',
                'totalArea' => 'required|min:1',
                'livingArea' => 'required|min:1',
                'kitchenArea' => 'required|min:1',
            ]);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://tt-api.tech/1.0/profanity?lang=rus&txt=".$request->name . ' . ' . explode("\n", trim($request->description), 2)[0],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Token RqwwK8UM9sRHkh4IdmuIGl3V0PRpbw1500V"
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);

            if ($response[19] == 0) {
                $post = new Product;
                $post->name = $request->name;
                $post->userId = $_SESSION['userId'];
                $post->description = $request->description;
                $post->category = $request->category;
                $post->type = $request->type;
                $post->price = $request->price;
                $post->locality = $request->locality;
                $post->district = $request->district;
                $post->countRooms = $request->countRooms;
                $post->floors = $request->floors;
                $post->floor = $request->floor;
                $post->totalArea = $request->totalArea;
                $post->livingArea = $request->livingArea;
                $post->facilities = $request->facilities ? $request->facilities : '';
                $post->kitchenArea = $request->kitchenArea;
                $post->customerId = 0;
                $post->save();

                $id = $post->id;
                if ($request->photos[0]) {

                    $loop = Factory::create();
                    $tgLog = new TgLog("796374941:AAFQQ1Hq0tkJVsuprE06bAQAAWhBgos8W78", new HttpClientRequestHandler($loop));

                    $sendPhoto = new SendPhoto();
                    $sendPhoto->chat_id = "@Kvartirnik_2019";
                    $sendPhoto->parse_mode = 'HTML';
                    $sendPhoto->photo = new InputFile(__DIR__ . '/../../../public/' . $request->photos[0]);
                    $sendPhoto->caption = $request->name . PHP_EOL . PHP_EOL . explode("\n", wordwrap(trim($request->description), 200), 2)[0] . '...' . PHP_EOL . PHP_EOL . '<a href="http://kvartirnik.vysotsky.productions/product/' . $id . '">Посмотреть объявление</a>';
                    $promise = $tgLog->performApiRequest($sendPhoto);

                    $loop->run();
                    foreach ($request->photos as $key => $photo) {
                        if ($photo) {
                            $post = new Photo;
                            $post->productId = $id;
                            $post->photoNumber = $key;
                            $post->img = $photo;
                            $post->save();
                        }
                    }
                }
                return $id;
            } else{
                $id = "censure";
                return $id;
            }
        }
    }

    public function editProduct(Request $request)
    {
        if (isset($_SESSION['username'])) {
            $validatedData = $request->validate([
                'name' => 'required|max:50|min:5',
                'description' => 'required|min:5',
                'category' => 'required|min:1',
                'type' => 'required|min:1',
                'price' => 'required|min:1',
                'locality' => 'required|min:1',
                'district' => 'required|min:1',
                'countRooms' => 'required|min:1',
                'floors' => 'required|min:1',
                'floor' => 'required|min:1',
                'totalArea' => 'required|min:1',
                'livingArea' => 'required|min:1',
                'kitchenArea' => 'required|min:1',
            ]);

            Product::where([
                'userId' => $_SESSION['userId'],
                'customerId' => 0,
                'id' => $request->id,
            ])->update([
                'name' => $request->name,
                'description' => $request->description,
                'category' => $request->category,
                'type' => $request->type,
                'price' => $request->price,
                'locality' => $request->locality,
                'district' => $request->district,
                'countRooms' => $request->countRooms,
                'floors' => $request->floors,
                'floor' => $request->floor,
                'totalArea' => $request->totalArea,
                'livingArea' => $request->livingArea,
                'facilities' => $request->facilities,
                'kitchenArea' => $request->kitchenArea,
            ]);
            return $request->id;
        }
    }

    public function deleteProduct(Request $request)
    {
        Product::where(['id' => $request->id,])->delete();
    }

    public function addPhoto(Request $request)
    {
        $this->validate($request, [
            '0' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $path = '/storage/' . $request->file('0')->store('uploads', 'public');

        Photo::where([
            'productId' => $request->id,
            'photoNumber' => $request->photoNumber
        ])->delete();
        if ($request->id) {
            $post = new Photo;
            $post->productId = $request->id;
            $post->photoNumber = $request->photoNumber;
            $post->img = $path;
            $post->save();
        }
        return $path;
    }

    public function addComment(Request $request)
    {
        if (isset($_SESSION['userId'])) {
            $validatedData = $request->validate([
                'message' => 'required|min:5',
            ]);

            $post = new Comment();
            $post->userId = $_SESSION['userId'];
            $post->productId = $request->productId;
            $post->text = $request->message;
            $post->save();
        } else {
            $request->username = $request->email;
            $validatedData = $request->validate([
                'email' => 'required|unique:users|min:1',
                'username' => 'required|unique:users|min:1',
                'name' => 'required|min:3',
                'message' => 'required||min:5',
            ]);
            $post = new User;
            $post->name = $request->name;
            $post->surname = "";
            $post->phone = "";
            $post->email = $request->email;
            $post->username = $request->email;
            $pass = random_int(5);
            $post->password = Hash::make($pass);
            $post->password_reset = "";
            $post->save();

            $user = User::where([
                'username' => $request->email
            ])->get();

            if ($user[0]->username && Hash::check($pass, $user[0]->password)) {
                $_SESSION['username'] = $request->email;
                $_SESSION['userId'] = $user[0]->id;
                $_SESSION['name'] = $user[0]->name;
                $_SESSION['surname'] = $user[0]->surname;
            }

            $post = new Comment();
            $post->userId = $_SESSION['userId'];
            $post->productId = $request->productId;
            $post->text = $request->message;
            $post->save();

            return ['login' => $login, 'password' => $pass];
        }
    }

    public function checkLocality(Request $request)
    {
        $district = District::where(['localityId' => $request->city])->get();
        //print_r($district);
        return $district;
    }

    public function recommendProducts(Request $request)
    {
        $recommend = Product::where([
            'customerId' => 0,
        ])->orderBy('id', 'asc')->limit(6)->get();

        $floors = Floors::get();
        $locality = Locality::get();
        $district = District::get();
        $countRooms = CountRoom::get();

        foreach ($recommend as $rec) {
            $photos[$rec->id] = Photo::where([
                'productId' => $rec->id,
                'photoNumber' => 0,
            ])->first();
        }

        $facilities = Product::where([
            'customerId' => 0,
        ])->orderBy('id', 'desc')->limit(6)->get();

        foreach ($facilities as $fac) {
            $photos[$fac->id] = Photo::where([
                'productId' => $fac->id,
                'photoNumber' => 0,
            ])->first();
        }

        return view('index', ['recommend' => $recommend, 'facilities' => $facilities, 'photos' => $photos, 'floors' => $floors, 'locality' => $locality, 'district' => $district, 'countRooms' => $countRooms,]);

    }

    public function mySuggestions(Request $request)
    {
        if (!isset($_SESSION['page'])) {
            $_SESSION['page'] = 1;
        }
        $suggestions = Product::where([
            'customerId' => 0,
            'userId' => $_SESSION['userId'],
        ])->orderBy('updated_at', 'desc')->take(6)->get();

        $max_page = ceil(Product::where(['customerId' => 0,
                'userId' => $_SESSION['userId'],
            ])->count() / 6);

        $photos = [];
        foreach ($suggestions as $sug) {
            $photos[$sug->id] = Photo::where([
                'productId' => $sug->id,
                'photoNumber' => 0,
            ])->first();
        }

        return view('mySuggestions', ['suggestions' => $suggestions, 'photos' => $photos, 'max_page' => $max_page]);
    }

    public function mySugPag(Request $request)
    {
        $max_page = ceil(Product::where(['customerId' => 0,
                'userId' => $_SESSION['userId'],
            ])->count() / 6);

        if ($request->page <= $max_page && $request->page >= 1) {
            $_SESSION['page'] = $request->page;

            $suggestions = Product::where([
                'customerId' => 0,
                'userId' => $_SESSION['userId'],
            ])->orderBy('updated_at', 'desc')->take(6)->offset((6 * $request->page) - 6)->get();


            $photos = [];

            foreach ($suggestions as $sug) {
                $photos[$sug->id] = Photo::where([
                    'productId' => $sug->id,
                    'photoNumber' => 0,
                ])->first();
            }

            $data = [
                'suggestions' => $suggestions,
                'photos' => $photos,
                'page' => $_SESSION['page'],
                'max_page' => $max_page,
            ];
            return $data;
        }
    }


    public function AddToBucket(Request $request)
    {
        $product = Product::where([
            'id' => $request->id,
            'customerId' => 0,
        ])->first();

        $bucket = Bucket::where([
            'productId' => $request->id,
            'userId' => $_SESSION['userId'],
        ])->first();

        if ($product && !$bucket && $product->userId != $_SESSION['userId']) {
            $post = new Bucket();
            $post->productId = $request->id;
            $post->userId = $_SESSION['userId'];
            $post->customerId = $product->userId;
            $post->save();
        }
    }

    public function Bucket(Request $request)
    {
        $bucket = Bucket::where([
            'userId' => $_SESSION['userId'],
        ])->get();

        $allprice = 0;
        $product = [];
        $key = -1;
        foreach ($bucket as $key => $item) {
            $product[$key] = Product::where([
                'id' => $item->productId
            ])->first();
            $allprice += $product[$key]->price;
        }

        return view('/bucket', ['bucket' => $bucket, 'product' => $product, 'allprice' => $allprice, 'last' => $key + 1]);
    }

    public function DeleteFromBucket(Request $request)
    {
        $bucket = Bucket::where([
            'userId' => $_SESSION['userId'],
            'productId' => $request->id,
        ])->delete();

        return [true];
    }

    public function OrderFromBucket(Request $request)
    {
        $bucket = Bucket::where([
            'userId' => $_SESSION['userId'],
        ])->get();

        if (isset($bucket[0])) {
            $PurchProducts = '';
            $orders = [];

            foreach ($bucket as $product) {
                $orders[$product->userId]['productsId'] = ' ';
            }

            foreach ($bucket as $product) {
                $PurchProducts .= $product->productId . ' ';
                $orders[$product->userId]['customerId'] = $product->customerId;
                $orders[$product->userId]['productsId'] .= $product->productId . ' ';
                Product::where(['id' => $product->productId])->update(['customerId' => $_SESSION['userId']]);
            }

            Purchase::create([
                'userId' => $_SESSION['userId'],
                'productsId' => $PurchProducts,
            ]);
            foreach ($orders as $order) {
                Order::create([
                    'userId' => $order['customerId'],
                    'customerId' => $_SESSION['userId'],
                    'productsId' => $order['productsId'],
                ]);
            }

            Bucket::where([
                'userId' => $_SESSION['userId'],
            ])->delete();

            return $bucket;
        }
    }

    public function myPurchases(Request $request)
    {
        $purchases = Purchase::where([
            'userId' => $_SESSION['userId'],
        ])->get();
        $products = [];
        foreach ($purchases as $key => $purchase) {
            $productsId = explode(' ', $purchase->productsId);
            $purchase->price = 0;
            for ($i = 0; $i < count($productsId) - 1; $i++) {
                $products[$key][$productsId[$i]] = Product::where(['id' => $productsId[$i]])->first();
                $purchase->price += $products[$key][$productsId[$i]]->price;
            }
            $purchase->count = count($productsId) - 1;
        }
        return view('myPurchases', ['purchases' => $purchases, 'products' => $products]);
    }

    public function myOrders(Request $request)
    {
        $orders = Order::where([
            'userId' => $_SESSION['userId'],
        ])->get();
        $products = [];
        foreach ($orders as $key => $order) {
            $productsId = explode(' ', $order->productsId);
            for ($i = 1; $i < count($productsId) - 1; $i++) {
                $products[$key][$productsId[$i]] = Product::where(['id' => $productsId[$i]])->first();
            }
        }
        return view('myOrders', ['orders' => $orders, 'products' => $products,]);
    }
}
