<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Product;
use App\Photo;
use App\Locality;
use App\Category;
//use http\Env\Request;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\Debug\Tests\Fixtures\ToStringThrower;
use Illuminate\Support\Facades\Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function search(Request $request)
    {
        $request->page = 1;
        if (isset($_SESSION)) {
            $_SESSION['search-page'] = 1;
        }

        !isset($_SESSION['keyword']) ? $_SESSION['keyword'] = '':'';
        !isset($_SESSION['locality']) ? $_SESSION['locality'] = '' : '';
        !isset($_SESSION['locality_name']) ? $_SESSION['locality_name'] = '' : '';
        !isset($_SESSION['category']) ? $_SESSION['category'] = '%' : '';
        !isset($_SESSION['countRooms1']) ? $_SESSION['countRooms1'] = 1 : '';
        !isset($_SESSION['countRooms2']) ? $_SESSION['countRooms2'] = 5 : '';
        !isset($_SESSION['floor1']) ? $_SESSION['floor1'] = 1 : '';
        !isset($_SESSION['floor2']) ? $_SESSION['floor2'] = 9 : '';
        !isset($_SESSION['price1']) ? $_SESSION['price1'] = 1 : '';
        !isset($_SESSION['price2']) ? $_SESSION['price2'] = 9999999 : '';

        $products = Product::where(['customerId' => 0,]
        )->where('countRooms', '>=', (int)$_SESSION['countRooms1']
        )->where('countRooms', '<=', (int)$_SESSION['countRooms2']
        )->where('floor', '>=', (int)$_SESSION['floor1'] - 1
        )->where('floor', '<=', (int)$_SESSION['floor2'] - 1
        )->where('price', '>=', (int)$_SESSION['price1']
        )->where('price', '<=', (int)$_SESSION['price2']
        )->where('locality', 'LIKE', '%' . $_SESSION['locality'] . '%'
        )->where('category', 'LIKE', '%' .$_SESSION['category'] . '%'
        )->where('name', 'LIKE', '%'.$_SESSION['keyword'].'%'
        )->paginate(9);

        $photos = [];
        foreach ($products as $prod) {
            $photos = Product::find($prod->id)->photos()->where(['photoNumber' => 0,])->first();
        }

        $category = Category::get();
        return view('search', ['products' => $products, 'photos' => $photos, 'category' => $category,]);
    }

    public function searchButton(Request $request)
    {
        $request->page = 1;
        $_SESSION['search-page'] = 1;

        isset($request->keyword) ? $_SESSION['keyword'] = $request->keyword : $_SESSION['keyword'] = '';
        isset($request->locality) ? $_SESSION['locality_name'] = $request->locality : $_SESSION['locality_name']= '';
        isset($request->locality) ? $_SESSION['locality'] = Locality::where('name', 'LIKE', '%' . $request->locality . '%')->first()->id-1 : $_SESSION['locality'] = '';
        $request->category!='%'? $_SESSION['category'] = $request->category - 1 : $_SESSION['category']='%';
        isset($request->countRooms1) ? $_SESSION['countRooms1'] = $request->countRooms1 : $_SESSION['countRooms1'] = 1;
        isset($request->countRooms2) ? $_SESSION['countRooms2'] = $request->countRooms2 : $_SESSION['countRooms2'] = 6;
        isset($request->floor1) ? $_SESSION['floor1'] = $request->floor1 : $_SESSION['floor1'] = 1;
        isset($request->floor2) ? $_SESSION['floor2'] = $request->floor2 : $_SESSION['floor2'] = 9;
        isset($request->price1) ? $_SESSION['price1'] = $request->price1 : $_SESSION['price1'] = 1;
        isset($request->price2) ? $_SESSION['price2'] = $request->price2 : $_SESSION['price2'] = 9999999;

      // return[$_SESSION['keyword'],$_SESSION['locality'],$_SESSION['locality_name'],$_SESSION['category'],$_SESSION['countRooms1'],$_SESSION['countRooms2'],$_SESSION['floor1'],
       //     $_SESSION['floor2'],$_SESSION['price1'],$_SESSION['price2']];

        $products = Product::where(['customerId' => 0,]
        )->where('countRooms', '>=', (int)$_SESSION['countRooms1']
        )->where('countRooms', '<=', (int)$_SESSION['countRooms2']
        )->where('floor', '>=', (int)$_SESSION['floor1'] - 1
        )->where('floor', '<=', (int)$_SESSION['floor2'] - 1
        )->where('price', '>=', (int)$_SESSION['price1']
        )->where('price', '<=', (int)$_SESSION['price2']
        )->where('locality', 'LIKE', '%'.$_SESSION['locality'].'%'
        )->where('category', 'LIKE', $_SESSION['category']
        )->where('name', 'LIKE', '%'.$_SESSION['keyword'].'%'
        )->paginate(9);

        $photos = [];
        foreach ($products as $prod) {
            $photos[$prod->id] = Photo::where([
                'productId' => $prod->id,
                'photoNumber' => 0,
            ])->first();
        }
        return ['products' => $products, 'photos' => $photos, 'page' => $_SESSION['search-page'],];
    }

    public function searchPag(Request $request)
    {
        $max_page = ceil(Product::where(['customerId' => 0,]
        )->where('countRooms', '>=', (int)$_SESSION['countRooms1']
        )->where('countRooms', '<=', (int)$_SESSION['countRooms2']
        )->where('floor', '>=', (int)$_SESSION['floor1'] - 1
        )->where('floor', '<=', (int)$_SESSION['floor2'] - 1
        )->where('price', '>=', (int)$_SESSION['price1']
        )->where('price', '<=', (int)$_SESSION['price2']
        )->where('locality', 'LIKE', '%'.$_SESSION['locality'].'%'
        )->where('category', 'LIKE', $_SESSION['category']
        )->where('name', 'LIKE', '%'.$_SESSION['keyword'].'%'
        )->count());

        if ($request->page > 0 && $request->page <= $max_page) {
            $_SESSION['search-page'] = $request->page;
        }

        $products = Product::where(['customerId' => 0,]
        )->where('countRooms', '>=', (int)$_SESSION['countRooms1']
        )->where('countRooms', '<=', (int)$_SESSION['countRooms2']
        )->where('floor', '>=', (int)$_SESSION['floor1'] - 1
        )->where('floor', '<=', (int)$_SESSION['floor2'] - 1
        )->where('price', '>=', (int)$_SESSION['price1']
        )->where('price', '<=', (int)$_SESSION['price2']
        )->where('locality', 'LIKE', '%'.$_SESSION['locality'].'%'
        )->where('category', 'LIKE', '%'. $_SESSION['category'] .'%'
        )->where('name', 'LIKE', '%'.$_SESSION['keyword'].'%'
        )->paginate(9);
        $photos = [];
        foreach ($products as $prod) {
            $photos[$prod->id] = Photo::where([
                'productId' => $prod->id,
                'photoNumber' => 0,
            ])->first();

        }

        return [
            'products' => $products,
            'photos' => $photos,
            'max_page' => $max_page,
            'page' => $_SESSION['search-page']
        ];
    }
    public function searchSession(Request $request){
        if($request->name == 'category' || $request->name == 'keyword') {
            $_SESSION[$request->name] = $request->el;
        }
    }

    public function users(Request $request)
    {
        if (isset($_SESSION)) {
            $_SESSION['page-user' . $request->id] = 1;
        }
        $user = User::where(['id' => $request->id])->first();

        $countProducts = Product::where([
            'customerId' => 0,
            'userId' => $request->id,
        ])->count();


        $products = Product::where([
            'customerId' => 0,
            'userId' => $request->id,
        ])->orderBy('updated_at', 'asc')->take(6)->get();

        $photos = [];

        foreach ($products as $prod) {
            $photos[$prod->id] = Photo::where([
                'productId' => $prod->id,
                'photoNumber' => 0,
            ])->first();
        }

        $max_page = ceil(Product::where([
                'customerId' => 0,
                'userId' => $request->id,
            ])->count() / 6);

        return view('landlord', ['user' => $user, 'countProducts' => $countProducts, 'products' => $products, 'photos' => $photos, 'max_page'=>$max_page]);
    }

    public function CreateUser(Request $request)
    {
        if (!isset($_SESSION['username'])) {
            $validatedData = $request->validate([
                'username' => 'required|unique:users|max:50|min:2',
                'password' => 'required|min:6',
            ]);
            if ($validatedData) {
                $post = new User;
                $post->name = "John";
                $post->surname = "Doe";
                $post->phone = "";
                $post->email = "";
                $post->username = $request->username;
                $post->password = Hash::make($request->password);
                $post->password_reset = "";
                $post->user_status = 0;
                $post->save();

                $user = User::where([
                    'username' => $request->username
                ])->get();

                $_SESSION['userId'] = $user[0]->id;
                $_SESSION['username'] = $user[0]->username;
                $_SESSION['name'] = $user[0]->name;
                $_SESSION['surname'] = $user[0]->surname;
                $_SESSION['user_status'] = $user[0]->user_status;
            }
        }
    }

    public function AuthUser(Request $request)
    {
        if (!isset($_SESSION['username'])) {
            $results = User::where([
                'username' => $request->username,
            ])->get();
            if ($results[0]->username && Hash::check($request->password, $results[0]->password)) {
                $_SESSION['username'] = $request->username;
                $_SESSION['userId'] = $results[0]->id;
                $_SESSION['name'] = $results[0]->name;
                $_SESSION['surname'] = $results[0]->surname;
                $_SESSION['user_status'] = $results[0]->user_status;
                return $results[0];
            }
        }
    }

    public function RecoveryPassword(Request $request)
    {
        if (!isset($_SESSION['username'])) {
            $results = User::where([
                'email' => $request->email,
                'password' => $request->password
            ])->get();
            return $results;
        }
    }

    public function UserData(Request $request)
    {
        if (isset($_SESSION['username'])) {
            $results = User::where([
                'username' => $_SESSION['username']
            ])->get();
            if ($results) {
                return $results;
            }
        }
    }

    public function userPag(Request $request)
    {
        $max_page = ceil(Product::where([
                'customerId' => 0,
                'userId' => $request->id,
            ])->count() / 6);

        if ($request->page <= $max_page && $request->page >= 1) {
            $_SESSION['page-user' . $request->id] = $request->page;

            $userprod = Product::where([
                'customerId' => 0,
                'userId' => $request->id,
            ])->orderBy('updated_at', 'desc')->take(6)->offset((6 * $request->page) - 6)->get();


            $photos = [];

            foreach ($userprod as $prod) {
                $photos[$prod->id] = Photo::where([
                    'productId' => $prod->id,
                    'photoNumber' => 0,
                ])->first();
            }

            $data = [
                'userprod' => $userprod,
                'photos' => $photos,
                'page' => $_SESSION['page-user' . $request->id],
            ];
            return $data;
        }
    }

    public function PersonalData(Request $request)
    {
        $user = User::where([
            'username' => $_SESSION['username']
        ])->first();
        return view('personalData', ['user' => $user]);
    }

    public function EditUserData(Request $request)
    {
        if (isset($_SESSION['username'])) {
            $results = User::where([
                'username' => $_SESSION['username']
            ])->first();
            if ($request->name) {
                $results->name = $request->name;
                $_SESSION['name'] = $request->name;
            }
            if ($request->surname) {
                $results->surname = $request->surname;
                $_SESSION['surname'] = $request->surname;
            }
            if ($request->phone) {
                $results->phone = $request->phone;
            }
            if ($request->email) {
                $results->email = $request->email;
            }
            $results->save();
            return $results;
        }
    }

    public function Logout()
    {
        if (isset($_SESSION['userId'])) {
            unset($_SESSION['username']);
            unset($_SESSION['userId']);
        }
    }
}

