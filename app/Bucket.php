<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bucket extends Model
{
    public $timestamps = false;
    protected $table = 'bucket';
    protected $hidden = [
            'id',
    ];
}
