<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facilitie extends Model
{
    protected $table = 'facilities';
    public $timestamps = false;
}
