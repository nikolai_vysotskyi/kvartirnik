<?php

session_start();

Route::group(array('prefix' => 'api'), function () {
    Route::get('/user/create', 'Controller@CreateUser');
    Route::get('/user/auth', 'Controller@AuthUser');

    Route::get('/user/logout', 'Controller@Logout');
    Route::get('/user/editUser', 'Controller@EditUserData');
    Route::get('/user/Pag', 'Controller@userPag');

    Route::post('/products/add', 'ProductsController@AddProduct');
    Route::post('/products/edit', 'ProductsController@editProduct');
    Route::post('/products/addPhoto', 'ProductsController@addPhoto');
    Route::get('/products/delete', 'ProductsController@deleteProduct');
    Route::get('/products/mySugPag', 'ProductsController@mySugPag');
    Route::get('/products/checkLocality', 'ProductsController@checkLocality');

    Route::put('/products/addComment', 'ProductsController@AddComment');

    Route::get('/bucket/product/{id}', 'ProductsController@AddToBucket');
    Route::delete('/bucket/delete/{id}', 'ProductsController@DeleteFromBucket');
    Route::put('/bucket/order', 'ProductsController@OrderFromBucket');

    Route::get('/search/button/', 'Controller@searchButton');
    Route::get('/search/pag/', 'Controller@searchPag');
    Route::get('/search/session', 'Controller@searchSession');

});

if (isset($_SESSION['username'])) {
    Route::get('/user/{id}', 'Controller@Users');

    Route::get('/mySuggestions', 'ProductsController@mySuggestions');
    Route::get('/newAdd', 'ProductsController@addProducts');
    Route::get('/editAdd/{id}', 'ProductsController@editProducts');
    Route::get('/personalData', 'Controller@PersonalData');

    Route::get('/myPurchases', 'ProductsController@myPurchases');

    Route::get('/myOrders', 'ProductsController@myOrders');
    Route::get('/bucket', 'ProductsController@Bucket');
    Route::get('/search', 'Controller@search');
} else {

    Route::get('/search', 'Controller@search');
    Route::get('/{some}', function ($req) {
        if ($req != 'search') {
            return redirect('/');
        }
    });
}

Route::get('/', 'ProductsController@recommendProducts');
Route::get('/product/{id}', 'ProductsController@Product');

