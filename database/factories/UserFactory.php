<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/


$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'surname' => $faker->name,
        'phone' => $faker->e164PhoneNumber,
        'email' => $faker->email,
        'username'=> $faker->userName,
        'password' => bcrypt(str_random(10)),
        'password_reset' => 0,
        ];
});

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'userId' => random_int(1,10),
        'customerId'=> random_int(0,1),
        'name' => $faker->title,
        'description' => $faker->text(190),
        'category' => random_int(0,1),
        'type' => random_int(0,2),
        'price' => random_int(0,100000),
        'locality' => random_int(0,1),
        'district' => random_int(0,3),
        'countRooms' => random_int(1,5),
        'floors' => random_int(0,5),
        'floor' => random_int(0,8),
        'totalArea' => random_int(0,1000),
        'livingArea' => random_int(0,1000),
        'kitchenArea' => random_int(0,1000),
        'facilities' => random_int(0,17)
    ];
});

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'userId' => random_int(1,50),
        'productId' => random_int(1,50),
        'text' => $faker->text,
    ];
});

$factory->define(App\Photo::class, function (Faker $faker) {
    return [
        'productId' => random_int(1,50),
        'img' => $faker->imageUrl,
        'photoNumber' => random_int(0,7),
    ];
});

