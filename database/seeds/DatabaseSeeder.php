<?php

use App\User;
use App\Comment;
use App\Product;
use App\Photo;
use App\Floors;
use App\Floor;
use App\Category;
use App\District;
use App\Locality;
use App\CountRoom;
use App\Type;
use App\Facilitie;


use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        $this->call(LocalityTableSeeder::class);
        $this->call(DistrictTableSeeder::class);
        $this->call(CountRoomTableSeeder::class);
        $this->call(FloorsTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(TypeTableSeeder::class);
        $this->call(FacilitesTableSeeder::class);

        $this->call(UserTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(PhotoTableSeeder::class);
        $this->call(CommentTableSeeder::class);

        $this->command->info('Admin User created with username admin@admin.com and password admin');
        $this->command->info('Test User created with username user@user.com and password user');

        Model::reguard();
    }
}


class UserTableSeeder extends Seeder
{
    public function run()
    {
        \App\User::create([
            'name' => 'Test',
            'surname' => 'User',
            'phone' => '+380981039298',
            'username' => 'test_user',
            'email' => 'user@user.com',
            'password' =>Hash::make('user'),
            'password_reset' => 0,
            'user_status'=>0,
        ]);
        factory(App\User::class, 50)->create();
        \App\User::create([
            'name' => 'Admin',
            'surname' => 'User',
            'phone' => '+380964707630',
            'username' => 'admin_user',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin'),
            'password_reset' => 0,
            'user_status'=>0,
        ]);
    }
}

class ProductTableSeeder extends Seeder
{
    public function run()
    {
        factory(App\Product::class, 200)->create();
    }
}
class CommentTableSeeder extends Seeder
{
    public function run()
    {
        factory(App\Comment::class, 200)->create();
    }
}


class PhotoTableSeeder extends Seeder
{
    public function run()
    {
        factory(App\Photo::class, 200)->create();
    }
}

class TypeTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('type')->insert([
            'name' => 'Люкс',
        ]);
        DB::table('type')->insert([
            'name' => 'Обычный',
        ]);
    }
}

class LocalityTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('locality')->insert([
            'name' => 'Мариуполь',
        ]);
        DB::table('locality')->insert([
            'name' => 'Краматорск',
        ]);
    }
}

class DistrictTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('district')->insert([
            'name' => 'Центральный',
            'localityId' => 1
        ]);
        DB::table('district')->insert([
            'name' => 'Приморский',
            'localityId' => 1
        ]);
        DB::table('district')->insert([
            'name' => 'Левобережный',
            'localityId' => 2
        ]);
        DB::table('district')->insert([
            'name' => 'Кальмиусский',
            'localityId' => 2
        ]);
    }
}

class CountRoomTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('count_rooms')->insert([
            'number' => '1',
        ]);
        DB::table('count_rooms')->insert([
            'number' => '2',
        ]);
        DB::table('count_rooms')->insert([
            'number' => '3',
        ]);
        DB::table('count_rooms')->insert([
            'number' => '4',
        ]);
        DB::table('count_rooms')->insert([
            'number' => '5',
        ]);
    }
}

class FloorsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('floors')->insert([
            'number' => '1',
        ]);
        DB::table('floors')->insert([
            'number' => '2',
        ]);
        DB::table('floors')->insert([
            'number' => '3',
        ]);
        DB::table('floors')->insert([
            'number' => '4',
        ]);
        DB::table('floors')->insert([
            'number' => '5',
        ]);
        DB::table('floors')->insert([
            'number' => '9',
        ]);
    }
}

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('category')->insert([
            'name' => 'Квартира',
        ]);
        DB::table('category')->insert([
            'name' => 'Дом',
        ]);
        DB::table('category')->insert([
            'name' => 'Нежелые помещения',
        ]);
    }
}

class FacilitesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('facilities')->insert([
            'title' => 'Диван',
            'svg' => 'sofa',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Двухспальная кровать',
            'svg' => 'double-bed',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Мебель',
            'svg' => 'furniture',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Бесплатный Wi-Fi',
            'svg' => 'free-wifi',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Телевизор',
            'svg' => 'tv',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Фен',
            'svg' => 'hair-dryer',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Чайник',
            'svg' => 'kettle',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Лифт',
            'svg' => 'elevator',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Бойлер',
            'svg' => 'boiler',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Балкон',
            'svg' => 'windows',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Охрана',
            'svg' => 'security',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Кухня',
            'svg' => 'kitchen',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Детская комната',
            'svg' => 'children',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Парковка',
            'svg' => 'parking',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Бассейн',
            'svg' => 'swimming-pool',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Тренажер',
            'svg' => 'gym',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Ремонт',
            'svg' => 'repairs',
        ]);
        DB::table('facilities')->insert([
            'title' => 'Кондиционер',
            'svg' => 'conditioner',
        ]);
    }
}
